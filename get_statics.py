import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
from matplotlib import pyplot as plt

#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data import imageset_fashion_patch_roi_pair, patch_roi_pair_blobs, imageset_fashion_patch_roi, patch_roi_blobs
from rcnn_dnn.networks import networks

import torch


def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

def get_statistics(dataset, patch_size, name):
    total_area = 0
    total_width = 0
    total_height = 0
    number = 0
    width_all = []
    height_all = []
    area_all = []
    for idx in range( len(dataset) ):
        inputs, targets = dataset.next()
        boxes = inputs['boxes']
        for box_im in boxes:
            box_im = box_im.numpy()
            for box in box_im:
                width = box[2] - box[0]
                height = box[3] - box[1]
                area = width * height
                total_area += area / 10000.0
                total_width += width/10
                total_height += height/10
                number += 1
                width_all.append(width)
                height_all.append(height)
                area_all.append(area)
    print('average_area is {}'.format(total_area/number*10000))
    print('average_width is {}'.format(total_width/number*10))
    print('average_height is {}'.format(total_height/number*10))
    print('total box number is {}'.format(number))
    area_path = './test_im/{}_area_hist.jpg'.format(name)
    width_path = './test_im/{}_width_hist.jpg'.format(name)
    height_path = './test_im/{}_height_hist.jpg'.format(name)
    area_title = 'Histgram of box area for {} with resolution {}'.format(name, patch_size)
    width_title = 'Histgram of box width for {} with resolution {}'.format(name, patch_size)
    height_title = 'Histgram of box height for {} with resolution {}'.format(name, patch_size)
    draw_hist(area_all, area_path, 'area', title=area_title)
    draw_hist(width_all, width_path, 'width', title=width_title)
    draw_hist(height_all, height_path, 'height', title=height_title)

def draw_hist(array, save_path, xlabel, title='Histgram'):
    the_array=np.array(array)
    max_val = np.ceil(np.amax(the_array)/10.0)*10
    min_val = np.floor(np.amin(the_array)/10.0)*10
    if xlabel=='area':
        interval = np.arange(min_val, max_val, step=10000)
    else:
        interval = np.arange(min_val, max_val, step=10)
    plt.figure()
    arr = plt.hist(the_array, bins=interval)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel('box number')
    for i in range(len(interval)):
        plt.text(arr[1][i],arr[0][i],str(arr[0][i]))
    plt.savefig(save_path, dpi=300)


if __name__=="__main__" :
    training = True
    params = {}
    params['config'] = 'patch_pair_roi_resnet'
    params['batch_size'] = 24
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20191002_roi_pair'
    params['path'] = {}

    #criterias = ['match']
    #criterias = ['category_accuracy', 'pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['top_k_retrieval_accuracy']
    #criterias = ['category_accuracy']
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    print(cfg.dnn.PATCH_SIZE)
    if hasattr(cfg.dnn, 'PATCH_SIZE'):
        patch_size = cfg.dnn.PATCH_SIZE
    elif hasattr(cfg.dnn, 'MIN_SIZE'):
        patch_size = cfg.dnn.MIN_SIZE
    else:
        raise ValueError('there is no valid patch size configuration')


    #get workbench
    train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='vgg16' )

    test_blob_gen = patch_roi_blobs(cfg.dnn)

    test_imageset_shop = imageset_fashion_patch_roi(cfg, test_dataset, test_blob_gen, mode='shop', randomize=False )
    test_imageset_user = imageset_fashion_patch_roi(cfg, test_dataset, test_blob_gen, mode='user', randomize=False )
    train_imageset_shop = imageset_fashion_patch_roi(cfg, train_dataset, test_blob_gen, mode='shop', randomize=False )
    train_imageset_user = imageset_fashion_patch_roi(cfg, train_dataset, test_blob_gen, mode='user', randomize=False )
    train_imageset_all = imageset_fashion_patch_roi(cfg, train_dataset, test_blob_gen, mode='all', randomize=False )

    #print('statistics for shop test:')
    #get_statistics(test_imageset_shop, patch_size, 'validation_shop')
    print('______________________________________________')
    print('statistics for user test:')
    get_statistics(test_imageset_user, patch_size, 'validation_user')
    #print('______________________________________________')
    #print('statistics for shop train:')
    #get_statistics(train_imageset_shop, patch_size, 'train_shop')
    #print('______________________________________________')
    #print('statistics for user train:')
    #get_statistics(train_imageset_user, patch_size, 'train_user')
    print('______________________________________________')
    print('statistics for all val:')
    get_statistics(train_imageset_all, patch_size, 'val_all')
    print('______________________________________________')
    print('statistics for all train:')
    get_statistics(train_imageset_all, patch_size, 'train_all')
