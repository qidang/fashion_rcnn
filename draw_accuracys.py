import _init
import os
from tools.visulize_tools import draw_accuracy_all, draw_pair_class_accuracy

def draw_mnist():
    names = ['mnist_A','mnist_B','mnist_C', 'mnist_D']
    file_names=[
        'bench_log_vgg16_small_vgg16_20190725_mnist_A.csv',
        'bench_log_vgg16_small_vgg16_20190725_mnist_C.csv',
        'bench_log_vgg16_small_vgg16_20190725_mnist_B.csv',
        'bench_log_vgg16_small_vgg16_20190725_mnist_D.csv']
    root_path = '/home/qi/Vision/data/embedding/mnist/benchmark'
    out_path = 'test/result_{}_all_mnist.jpg'
    assert len(names) == len (file_names)

    log_pathes = []
    for i in range(len(names)):
        log_pathes.append(os.path.join(root_path, file_names[i]))
    draw_accuracy_all(log_pathes, names, out_path,interval=0, type_num=1)

def draw_mnist_pair():
    name = 'accuracy'
    type_names=['category', 'match', 'sample5 top3', 'sample5 top10', 'sample10 top3', 'sample10 top10', 'sample20 top3', 'sample20 top10']
    ignore_names = ['category', 'sample5 top10']
    type_num = len(type_names)
    file_names=[
        'bench_log_vgg16_small_EasyNetD_20190806_fahshion_mnist_D_match_test.csv']
    root_path = '/home/qi/Vision/data/embedding/mnist_pair/benchmark'
    out_path = 'test/result_{}_all_fashion_mnist.jpg'

    log_path = os.path.join(root_path, file_names[0])
    draw_pair_class_accuracy(log_path, name, out_path,interval=0, type_num=type_num, type_names=type_names, ignore_names=ignore_names)

def draw_cifar_pair_new():
    name = 'accuracy'
    type_names=['category', 'match', 'sample5 top3', 'sample5 top10', 'sample10 top3', 'sample10 top10', 'sample20 top3', 'sample20 top10']
    ignore_names = ['category', 'sample5 top10']
    type_num = len(type_names)
    file_names=[
        'bench_log_vgg16_small_vgg16_bn_20190806_cifar10_l1.csv']
    root_path = '/home/qi/Vision/data/embedding/cifar10_pair/benchmark'
    out_path = 'test/result_{}_all_cifar10_new.jpg'

    log_path = os.path.join(root_path, file_names[0])
    draw_pair_class_accuracy(log_path, name, out_path,interval=0, type_num=type_num, type_names=type_names, ignore_names=ignore_names)
def draw_cifar_pair():
    names = ['match_l1', 'match_l2']
    type_names=['match', 'category']
    type_num = len(type_names)
    file_names=[
        'bench_log_vgg16_small_vgg16_bn_20190731_cifar10_l1.csv',
        'bench_log_vgg16_small_vgg16_bn_20190731_cifar10_l2.csv']
    root_path = '/home/qi/Vision/data/embedding/cifar10_pair/benchmark'
    out_path = 'test/result_{}_all_cifar10.jpg'
    assert len(names) == len (file_names)

    log_pathes = []
    for i in range(len(names)):
        log_pathes.append(os.path.join(root_path, file_names[i]))
    draw_accuracy_all(log_pathes, names, out_path,interval=0, type_num=type_num, type_names=type_names)

def draw_fashion_mnist_pair():
    names = ['match_l1', 'match_l2']
    type_names=['match', 'category']
    type_num = len(type_names)
    file_names=[
        'bench_log_vgg16_small_EasyNetD_20190730_fahshion_mnist_D_match_l1.csv', 
        'bench_log_vgg16_small_EasyNetD_20190730_fahshion_mnist_D_match_l2.csv']
    root_path = '/home/qi/Vision/data/embedding/mnist_pair/benchmark'
    out_path = 'test/result_{}_all_fashion_mnist.jpg'
    assert len(names) == len (file_names)

    log_pathes = []
    for i in range(len(names)):
        log_pathes.append(os.path.join(root_path, file_names[i]))
    draw_accuracy_all(log_pathes, names, out_path,interval=0, type_num=type_num, type_names=type_names)

def draw_fashion_mnist():
    names=['fashion_A','fashion_B','fahsion_C','fashion_D']
    file_names=[
        'bench_log_vgg16_small_vgg16_20190725_fashion_mnist_A.csv',
        'bench_log_vgg16_small_vgg16_20190725_fashion_mnist_B.csv',
        'bench_log_vgg16_small_vgg16_20190725_fashion_mnist_C.csv',
        'bench_log_vgg16_small_vgg16_20190725_fashion_mnist_D.csv']
    root_path = '/home/qi/Vision/data/embedding/mnist/benchmark'
    out_path = 'test/result_all_fashion_mnist.jpg'
    assert len(names) == len (file_names)

    log_pathes = []
    for i in range(len(names)):
        log_pathes.append(os.path.join(root_path, file_names[i]))
    draw_accuracy_all(log_pathes, names, out_path,interval=0)

def draw_cifar10():
    names=['VGG16_BN', 'VGG16', 'VGG_small', 'EasyNetA','EasyNetB', 'EasyNetC', 'EasyNetD']
    file_names=[
        'bench_log_vgg16_small_vgg16_20190729_cifar10_VGG16_BN.csv',
        'bench_log_vgg16_small_vgg16_20190725_cifar10_vgg16.csv',
        'bench_log_vgg16_small_vgg16_20190725_cifar10_vgg_samll.csv',
        'bench_log_vgg16_small_vgg16_20190728_cifar10_EasyNetA.csv',
        'bench_log_vgg16_small_vgg16_20190728_cifar10_EasyNetB.csv',
        'bench_log_vgg16_small_vgg16_20190728_cifar10_EasyNetC.csv',
        'bench_log_vgg16_small_vgg16_20190728_cifar10_EasyNetD.csv'
    ]
    root_path = '/home/qi/Vision/data/embedding/cifar10/benchmark'
    out_path = 'test/result_all_cifar10.jpg'
    assert len(names) == len (file_names)

    log_pathes = []
    for i in range(len(names)):
        log_pathes.append(os.path.join(root_path, file_names[i]))
    draw_accuracy_all(log_pathes, names, out_path,interval=0)

if __name__=='__main__':
    #draw_cifar10()
    #draw_cifar_pair()
    draw_cifar_pair_new()
    #draw_mnist()
    #draw_fashion_mnist()
