import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger

#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw
import tensorflow as tf

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data import roi_pair_blobs, imageset_fashion_roi_pair, patch_pair_blobs, roi_blobs,imageset_fashion_roi, imageset_fashion_hdf5
from rcnn_dnn.networks import networks

import torch
import torchvision.transforms as transforms
import torch.optim as optim

from dnn import trainer
from benchmark import Benchmark

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, benchset=None, benchmark=None, criterias=None, feature_set=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model
        self._feature_set = feature_set

        self._trainset = trainset
        self._testset = testset
        self._benchset = benchset
        self._benchmark = benchmark
        self._criterias = criterias
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._niter ):
            print("Epoch %d/%d" % (i+1,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None :
                self._validate()

    def _validate( self ):
        print('start to validate')
        self._model['net'].eval()

        bench = self._benchmark

        if 'top_k_retrieval_accuracy' in bench._criterias:
            self.get_bench_embedding()
            valid_uids = self._benchset.valid_uids
            top_k_param = {'k':[1, 5, 10, 15, 20], 'embeddings': self._bench_embedding, 'embedding_uids': self._bench_uid, 'valid_uids': valid_uids}
            param={'top_k_retrieval_accuracy':top_k_param}
        else:
            param = {}
        self._benchmark.update_parameters(param)
        

        with torch.no_grad() :
            test_len = len(self._testset)
            for idx in range( test_len ):
                inputs, targets = self._testset.next()
                inputs = self._set_device( inputs )
                output = self._model['net']( inputs, just_embedding=True)
                bench.update( targets, output )
                if idx % 1000 == 0:
                    print('{}  test {}/{}'.format(datetime.datetime.now(),idx, test_len))

        bench.summary()

    def get_bench_embedding(self):
        self._model['net'].eval()
        self._bench_embedding = []
        self._bench_uid = []
        with torch.no_grad() :
            bench_len = len(self._benchset)
            for idx in range( bench_len ):
                inputs, targets = self._benchset.next()
                inputs = self._set_device( inputs )
                output = self._model['net']( inputs, just_embedding=True )
                embedding = output['embeddings']
                uid = targets['uid']
                self._bench_embedding.append(embedding.detach().cpu().numpy())
                self._bench_uid.append(uid.numpy())
        self._bench_embedding = np.concatenate(self._bench_embedding, axis=0 )
        self._bench_uid = np.concatenate(self._bench_uid, axis=0)
                

    def _train( self ):
        self._model['net'].train()

        #widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
        #            '(',progressbar.DynamicMessage('loss'),')' ]
        #bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []
        average_losses = {}

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            outputs = self._model['net']( inputs)
            losses = self._model['loss']( outputs, targets )
            # add the losses for each part
            loss_sum = 0
            for single_loss in losses:
                loss_sum += losses[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= losses[single_loss]
                else:
                    average_losses[single_loss] = losses[single_loss]


            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%100 == 0:
                self._logger.info('{} '.format(idx+1))
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    print(' {} loss:{}'.format(loss, loss_num))
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        for n,d in blobs.items() :
            if type(d) == list:
                for i, item in enumerate(d):
                    #self._set_device(item)
                    blobs[n][i] = item.to(self._device)
            else:
                blobs[n] = d.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model['net'].parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model['net'].parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def get_features(self):
        #model_path = cfg.path.MODEL
        #self.load_trained_model(model_path)
        self._model['net'].eval()

        with torch.no_grad() :
            test_len = len(self._feature_set)
            for idx in range( test_len ):
                inputs, targets = self._feature_set.next()
                label = targets['category'].detach().cpu().numpy()
                inputs = self._set_device( inputs )
                output = self._model['net']( inputs, just_embedding=True)
                feature = output['embeddings'].detach().cpu().numpy()
                if idx == 0:
                    feature_all = feature
                    labels = label
                else:
                    feature_all = np.concatenate((feature_all,feature), axis=0)
                    labels = np.concatenate((labels,label), axis=0)
        return {'features':feature_all, 'labels':labels}

    def save_features(self):
        features = self.get_features()
        feature_file = 'fashion_patch_feature/feature_{}d_fashion_patch.pkl'.format(self._cfg.dnn.NETWORK.FEATURE_DIM)
        with open(feature_file,'wb') as f:
            pickle.dump(features, f)
            print('features are saved to {}'.format(feature_file))

if __name__=="__main__" :
    params = {}
    params['config'] = 'pair_roi_resnet'
    params['batch_size'] = 4
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20191008_pair'
    params['path'] = {}

    #criterias = ['match']
    #criterias = ['category_accuracy', 'pair_match_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['pair_match_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['category_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['top_k_retrieval_accuracy']
    criterias = ['category_accuracy']
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )


    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    #test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='resnet_50' )

    benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None

    blob_gen = roi_pair_blobs(cfg.dnn)
    test_blob_gen = roi_blobs(cfg.dnn)

    train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    test_dataset = load_dataset(cfg, ['deepfashion2', 'validation'])
    #blobs_gen = patch_pair_blobs(cfg.dnn)
    #test_imageset = imageset_fashion_triplet_hdf5(cfg, is_training=False, randomize=False, random_sample_num=5120 )
    #train_imageset = imageset_fashion_triplet_hdf5(cfg, is_training=True, randomize=True, random_sample_num=102400)
    train_imageset = imageset_fashion_roi_pair(cfg, train_dataset, blob_gen, randomize=True, is_training=True, random_sample_num= 102400 )
    #train_imageset = imageset_fashion_pair_hdf5(cfg, is_training=True, randomize=True, sample_num_pos=102400)
    #test_imageset = imageset_fashion_pair_hdf5(cfg, is_training=True, randomize=True, sample_num_pos=10240)

    #test_imageset_shop = imageset_fashion_roi(cfg, test_dataset, test_blob_gen, is_training=False, mode='shop')
    #test_imageset_user = imageset_fashion_roi(cfg, test_dataset, test_blob_gen, is_training=False, mode='user')
    test_imageset_all = imageset_fashion_roi(cfg, test_dataset, test_blob_gen, is_training=False, mode='all')

    train_feeder = data_feeder(train_imageset)
    #test_feeder = data_feeder(test_imageset)
    #test_feeder = None

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    model['net'] = networks.pair_roi.net( cfg ).to( device )
    model['loss'] = networks.pair_roi.loss(cfg).to(device)
    #model['net'] = networks.category_embedding.net(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)

    t = my_trainer( cfg, model, device, train_feeder, testset=test_imageset_all, benchset=None, benchmark=benchmark )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    t.train()

    train_feeder.exit()
    #test_feeder.exit()

    torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)
