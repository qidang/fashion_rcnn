import _init
from tools.visualize_2d_scatter import visualize_2d_scatter

def draw_mnist():
    ori_dim = 64
    data_path='mnist_feature/feature_{}d_mnist.pkl'.format(ori_dim)
    out_path = 'test/mnist_{}d.jpg'.format(ori_dim)
    dataset = 'MNIST'
    visualize_2d_scatter(data_path, out_path, dataset, ori_dim)

def draw_fashion_mnist(ori_dim):
    data_path='fashion_mnist_feature/feature_{}d_fashion_mnist.pkl'.format(ori_dim)
    out_path = 'test/fashion_mnist_{}d.jpg'.format(ori_dim)
    dataset = 'Fashion MNIST'
    visualize_2d_scatter(data_path, out_path, dataset, ori_dim)

def draw_cifar10(ori_dim):
    data_path='cifar10_feature/feature_{}d_cifar10.pkl'.format(ori_dim)
    out_path = 'test/cifar10_{}d.jpg'.format(ori_dim)
    dataset = 'Cifar 10'
    visualize_2d_scatter(data_path, out_path, dataset, ori_dim)

def draw_cifar10_triplet(ori_dim, hard_mining=False):
    if hard_mining:
        data_path='cifar10_triplet_feature/feature_{}d_triplet_hard_mining_cifar10.pkl'.format(ori_dim)
        out_path = 'test/cifar10_triplet_hardmining_{}d.jpg'.format(ori_dim)
    else:
        data_path='cifar10_triplet_feature/feature_{}d_triplet_cifar10.pkl'.format(ori_dim)
        out_path = 'test/cifar10_triplet_{}d.jpg'.format(ori_dim)
    dataset = 'Cifar 10'
    visualize_2d_scatter(data_path, out_path, dataset, ori_dim, hard_mining=hard_mining)

if __name__ == '__main__':
    ori_dim = 4
    draw_fashion_mnist(4)