import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np

#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw
import tensorflow as tf

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data import pair_blobs, imageset_retrieval
from rcnn_dnn.networks import networks

import torch
import torchvision.transforms as transforms
import torch.optim as optim

from dnn import trainer

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def _train( self ):
        self._model['loss'].train()

        widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
                    '(',progressbar.DynamicMessage('loss'),')' ]
        bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []
        average_losses = {}

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            #outputs = self._model['net']( inputs)
            losses = self._model['loss']( inputs, targets )
            # add the losses for each part
            loss_sum = 0
            for single_loss in losses:
                loss_sum += losses[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= losses[single_loss]
                else:
                    average_losses[single_loss] = losses[single_loss]

            loss_values.append( loss_sum.cpu().detach().numpy() )

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()

            if idx%100 == 0:
                for loss in average_losses:
                    print('{}:{}'.format(loss, average_losses[loss]))
                average_losses = {}

            bar.update(idx+1,loss=loss_sum.item())
        bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        for n,d in blobs.items() :
            if type(d) == list:
                for item in d:
                    self._set_device(item)
            else:
                blobs[n] = d.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        self._optimizer = optim.SGD( self._model['loss'].parameters(),
                                     lr=params['lr'],
                                     momentum=params.get('momentum',0.9),
                                     weight_decay=params.get('weight_decay',1e-4))

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )

        self._niter = self._cfg.dnn.NITER

#def get_dataset():
#  #args = parse_commandline()
#    params = parse_commandline()
#    params['config'] = 'patch_dataset'
#    params['gpu'] = '0'
#    params['path'] = {}
#    #path for save data
#    
#    if platform.system() == 'Linux':
#        params['path']['project'] = '/home/qi/Vision/data'
#    else:
#        params['path']['project'] = '/Users/qida0163/Vision/data'
#    #path to load data
#    if platform.system() == 'Linux':
#        params['path']['dataset'] = '/ssd/data'
#    else:
#        params['path']['dataset'] = '/Users/qida0163/Vision/data'
#
#    device = torch_tools.get_device( params['gpu'] )
#
#    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
#    cfg.update( params )
#
#    #get workbench
#    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
#    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
#    train_dataset = load_dataset( cfg, ['deepfashion','capb','train'] )
#
#    #set the paths to save all the results (model, val result)
#    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )
#
#    train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)
#
#    train_feeder = data_feeder(train_imageset)
#
#    return train_feeder

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    #params = parse_commandline()
    #params['config'] = 'patch_dataset'
    #params['gpu'] = '0'
    #params['path'] = {}
    ##path for save data
    
    #if platform.system() == 'Linux':
    #    params['path']['project'] = '/home/qi/Vision/data'
    #else:
    #    params['path']['project'] = '/Users/qida0163/Vision/data'
    ##path to load data
    #if platform.system() == 'Linux':
    #    params['path']['dataset'] = '/ssd/data'
    #else:
    #    params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    #cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    #cfg.update( params )

    ##get workbench
    ##trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    ##testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion','capb','train'] )

    ##set the paths to save all the results (model, val result)
    #cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    #train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)

    #train_feeder = data_feeder(train_imageset)
    params = {}
    params['config'] = 'rcnn_vgg16'
    params['batch_size'] = 4
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    train_dataset = load_dataset( cfg, ['deepfashion2','train'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    blobs_gen = pair_blobs(cfg.dnn)
    train_imageset = imageset_retrieval(cfg, train_dataset, blobs_gen, is_training=True, randomize=True)

    train_feeder = data_feeder(train_imageset)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    #model['net'] = networks['gtbox'].Net( cfg ).to( device )
    model['loss'] = networks.fashion_rcnn.loss( cfg ).to( device )
    #model['net'] = networks.category_embedding.net(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)

    t = my_trainer( cfg, model, device, train_feeder )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    t.train()

    train_feeder.exit()

    torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)
