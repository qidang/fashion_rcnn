from benchmark import Benchmark
from benchmark.bench_criterias import bench_criterias

class RetrievalClassificationBench(Benchmark):
    def __init__(self, cfg, criterias, k_vec ):
        self._criterias = criterias
        self._cfg = cfg
        self._k_vec = k_vec
        self.init_logger()
        self.init_bench()

    def init_bench(self):
        self._bench = []
        for criteria in self._criterias:
            if criteria not in bench_criterias:
                raise ValueError('The criteria {} is not supported, the supported ones are: {}'.format(criteria, list(bench_criterias.keys())))
            elif criteria=='retrieval_classification_accuracy':
                self._bench.append(bench_criterias[criteria](self._cfg, self._k_vec, self._logger))
            else:
                self._bench.append(bench_criterias[criteria](self._cfg, self._logger))
