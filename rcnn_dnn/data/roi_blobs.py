import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image
import random

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class roi_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _find_scale(self, width, height, min_size, max_size):
            max_side = max(width, height)
            min_side = min(width, height)
            scale = float(min_size) / min_side
            if max_side * scale > max_size:
                scale = float(max_size) / max_side
            return scale

    def _cal_padding(self, images, size_devisable=32.0):
        max_size = tuple(max(s) for s in zip(*[image.shape for image in images]))
        stride = size_devisable

        max_size = list(max_size)
        max_size[0] = int(math.ceil(max_size[0] / stride) * stride)
        max_size[1] = int(math.ceil(max_size[1] / stride) * stride)

        for image in images:
            image.padding_y = int(max_size[0] - image.height)
            image.padding_x= int(max_size[1] - image.width)


    def _padding_batches(self, images):
        '''
        split the images to two groups as image pairs and 
        padding each group to same size
        '''
        images1 = []
        images2 = []
        for i, image in enumerate(images):
            if i%2 == 0:
                images1.append(image)
            else:
                images2.append(image)
        self._cal_padding(images1)
        self._cal_padding(images2)
        
    def _add_data( self, images, blobs, training ):
        data = []

        min_size = self._dnn_cfg.MIN_SIZE
        max_size = self._dnn_cfg.MAX_SIZE

        # find scale for each image and padding for each image
        for i, image in enumerate(images) :
            #resize the image using scale in im_PIL
            width = image.ori_width
            height = image.ori_height
            scale = self._find_scale(width, height, min_size, max_size) 
            image.scale = scale
            # important: reset padding here so next time calulate padding for different batches
            image.padding_x = 0
            image.padding_y = 0

        # padding image to same size to form batches
        # padding is operated at right down side
        #self._padding_batches(images)
        self._cal_padding(images)

        for i, image in enumerate(images) :
            #im = image.im_PIL.convert('RGB') # the image scale and padding are excuted here
            im = image.im_PIL # the image scale and padding are excuted here

            pt_im = self._toTensor( im )
            pt_im = self._normalizeTensor( pt_im )
            data.append(pt_im)

        blobs['data'] = torch.stack(data)
        #blobs['images'] = the_images #debug

    def _add_groundtruth(self, images, blobs, training):
        boxes = []
        cat_labels = [] # category labels

        #gtbatches = [[],[]]

        for i,image in enumerate( images ) :
            im_boxes = image.gtboxes
            im_labels = image.gtlabels.reshape((-1))

            if len( im_boxes ) > 0 :
                boxes.append(torch.from_numpy(im_boxes))
                cat_labels.append(im_labels)

        cat_labels = np.concatenate(cat_labels).astype(int)
        cat_labels = torch.from_numpy(cat_labels) - 1

        blobs['boxes'] = boxes
        blobs['cat_labels'] = cat_labels

    def _add_unique_ids(self, images, blobs):
        unique_ids = []
        for image in images:
            unique_ids.append(image.unique_ids)
        unique_ids = np.concatenate(unique_ids, axis=-1)
        blobs['unique_id'] = torch.from_numpy(unique_ids)

    def get_blobs( self, images, training ):
        blobs = {}

        # Converting the data to the propper format
        self._add_data( images, blobs, training )
        self._add_groundtruth( images, blobs, training)
        self._add_unique_ids(images, blobs)

        inputs = {}
        inputs['data'] = blobs['data']
        inputs['boxes'] = blobs['boxes']

        targets = {}
        targets['category'] = blobs['cat_labels']
        targets['uid'] = blobs['unique_id']

        return inputs, targets
