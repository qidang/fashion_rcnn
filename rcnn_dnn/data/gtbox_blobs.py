import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms
from torch.autograd import Variable

class gtbox_blobs( data_blobs ):
    def __init__( self, cfg ):
        super().__init__( cfg )

        self._transforms = []
        self._transforms.append( transforms.ToTensor() )
        self._transforms.append( transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225]) )

    def _add_categories( self, images, blobs, training ):
        categories = []

        for image in images :
            c = int(image.category.ravel()[0])
            categories.append( c )

        gtcategory = np.array( categories, dtype=np.int64 )
        blobs['gtcategory'] = torch.from_numpy( gtcategory )

    def get_blobs( self, images, training ):
        blobs = {}

        # Converting the data to the propper format
        self._add_data( images, blobs )
        self._add_gtboxes( images, blobs )
        self._add_categories( images, blobs, training )

        inputs = {}
        inputs['data'] = Variable( blobs['data'], requires_grad=False )
        inputs['shapes'] = Variable( blobs['shapes'], requires_grad=False )
        inputs['scales'] = Variable( blobs['scales'], requires_grad=False )
        inputs['batch_labels'] = Variable( blobs['batch_labels'], requires_grad=False )
        inputs['gtboxes'] = Variable( blobs['gtboxes'], requires_grad=False )
        inputs['gtlabels'] = Variable( blobs['gtlabels'], requires_grad=False )
        inputs['gtbatches'] = Variable( blobs['gtbatches'], requires_grad=False )

        targets = {}
        targets['gtcategory'] = Variable( blobs['gtcategory'], requires_grad=False )

        return inputs, targets
