import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms
import random

class patch_pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_data( self, images, blobs, training ):
        data = []

        im_size = self._dnn_cfg.MAX_SIZE

        for image in images :
            assert len( image.gtboxes ) == 1
            im = image.im_PIL.convert('RGB')

            box = image.gtboxes[0]
            box = np.floor( box ).astype( int )

            # Extracting the patch
            patch = im.crop((box[0],box[1],box[2],box[3]))
            patch = patch.resize((im_size, im_size))

            pt_patch = self._toTensor( patch )
            pt_patch = self._normalizeTensor( pt_patch )

            data.append( pt_patch )

        blobs['data'] = torch.stack( data )

    def _add_categories( self, images, blobs, training ):
        categories = []

        for image in images :
            c = int(image.category.ravel()[0])
            categories.append( c )

        gtcategory = np.array( categories, dtype=np.int64 )
        blobs['gtcategory'] = torch.from_numpy( gtcategory )

    def _get_patch(self, image):
        im_size = self._dnn_cfg.MAX_SIZE
        assert len( image.gtboxes ) == 1
        im = image.im_PIL.convert('RGB')

        box = image.gtboxes[0]
        box = np.floor( box ).astype( int )

        # Extracting the patch
        patch = im.crop((box[0],box[1],box[2],box[3]))
        patch = patch.resize((im_size, im_size))

        pt_patch = self._toTensor( patch )
        pt_patch = self._normalizeTensor( pt_patch )
        return pt_patch

    def _get_patch_with_box(self, image, box):
        im_size = self._dnn_cfg.PATCH_SIZE
        im = image.im_PIL.convert('RGB')

        box = np.floor( box ).astype( int )

        # Extracting the patch
        patch = im.crop((box[0],box[1],box[2],box[3]))

        patch = patch.resize((im_size, im_size))

        pt_patch = self._toTensor( patch )
        pt_patch = self._normalizeTensor( pt_patch )
        return pt_patch
        #return patch


    def _add_pair_data(self, images, blobs, pair_labels):
        data1 = []
        data2 = []

        label1 = []
        label2 = []
        #for image in images:
        #    print('pair id is: ',image.pair_id)

        pair_num = int(len(images) / 2)
        for i in range(pair_num):
            image1 = images[2*i]
            image2 = images[2*i+1]
            
            styles1 = image1.styles
            styles2 = image2.styles
            categories1 = image1.categories
            categories2 = image2.categories
            pairs = []
            if pair_labels[i] == 1: #positive pairs
                assert image1.pair_id == image2.pair_id
                for i1, style1 in enumerate(styles1):
                    if style1 == 0:
                        continue
                    for i2, style2 in enumerate(styles2):
                        if style1==style2:
                            pairs.append((i1, i2))
                the_pair = random.choice(pairs)
            else:
                i1 = random.randrange(0,len(styles1))
                i2 = random.randrange(0,len(styles2))
                the_pair = (i1, i2)
            box1 = image1.boxes[the_pair[0]]
            box2 = image2.boxes[the_pair[1]]

            # make the category range start from 0
            category1 = categories1[the_pair[0]] - 1
            category2 = categories2[the_pair[1]] - 1

            patch1 = self._get_patch_with_box(image1, box1)
            patch2 = self._get_patch_with_box(image2, box2)
            #if pair_labels[i]==1:
            #    patch1.save('./test/pos{}_1.jpg'.format(i))
            #    patch2.save('./test/pos{}_2.jpg'.format(i))


            data1.append(patch1)
            data2.append(patch2)

            label1.append(category1)
            label2.append(category2)
        data1.extend(data2)
        category = np.array(label1+label2, dtype=np.long).flatten()
        #blobs['data'] = data1
        #blobs['category'] = category
        blobs['data'] = torch.stack(data1)
        blobs['category'] = torch.from_numpy(category)
                    
    #def get_blobs( self, images, training ):
    #    blobs = {}

    #    # Converting the data to the propper format
    #    self._add_data( images, blobs, training )
    #    self._add_categories( images, blobs, training )

    #    inputs = {}
    #    inputs['data'] = blobs['data']

    #    targets = {}
    #    targets['gtcategory'] = blobs['gtcategory']

    #    return blobs, targets

    def _add_pair_labels(self, labels, blobs):
        blobs['pair_labels'] = torch.from_numpy(labels)

    def get_blobs( self, images, labels, training ):
        assert len(images)==len(labels)
        pair_labels = labels.reshape((-1, 2))
        pair_labels = pair_labels[:,0]

        blobs = {}
        self._add_pair_data(images, blobs, pair_labels)
        self._add_pair_labels(pair_labels, blobs)

        inputs = {}
        inputs['data'] = blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        targets['pair_labels'] = blobs['pair_labels']
        return inputs, targets
