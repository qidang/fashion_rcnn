
import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class mnist_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=(0.1307,),
                                                     std=(0.3081,))

    def get_blobs(self, images, labels):
        im_all = []
        for im in images:
            #print(im.shape)
            #print(im.dtype)
            #im = (im.astype(np.float32)/255.0-0.5)/0.5
            im = im.astype(np.float32)/255.0
            
            #im_new = Image.fromarray(im.reshape((28,28)), mode='L')
            #break

            im = torch.from_numpy(im)
            #im = self._toTensor(im_new)
            #im = self._normalizeTensor(im)
            im_all.append(im)
        ims = torch.stack(im_all)

        labels = np.array(labels).astype(int)
        labels = torch.from_numpy(labels)

        inputs = {}
        inputs['data']=ims

        targets = {}
        targets['category'] = labels
        return inputs, targets