
import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class cifar10_triplet_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=(0.5, 0.5, 0.5),
                                                     std=(0.5, 0.5, 0.5))
        self._randomCrop = transforms.RandomCrop(32, padding=4)
        self._randomHorizontalFlip = transforms.RandomHorizontalFlip()

    def _im_to_tensor(self, im, training):
        im_new = Image.fromarray(im.reshape((3, 32, 32)).transpose([1,2,0]), mode='RGB')

        #im = torch.from_numpy(im)
        if training:
            im_new = self._randomCrop(im_new)
            im_new = self._randomHorizontalFlip(im_new)
        im = self._toTensor(im_new)
        im = self._normalizeTensor(im)
        return im


    def _add_triplet_data(self, images, cat_labels, blobs, training):
        data_a = []
        data_pos = []
        data_neg = []

        label_a = []
        label_pos = []
        label_neg = []
        #for image in images:
        #    print('pair id is: ',image.pair_id)

        tri_num = int(len(images) / 3)
        for i in range(tri_num):
            image_a = images[3*i]
            image_pos = images[3*i+1]
            image_neg = images[3*i+2]

            image_a = self._im_to_tensor(image_a, training)
            image_pos = self._im_to_tensor(image_pos, training)
            image_neg = self._im_to_tensor(image_neg, training)

            category_a = cat_labels[3*i]
            category_pos = cat_labels[3*i+1]
            category_neg = cat_labels[3*i+2]

            data_a.append(image_a)
            data_pos.append(image_pos)
            data_neg.append(image_neg)

            label_a.append(category_a)
            label_pos.append(category_pos)
            label_neg.append(category_neg)

        data_a.extend(data_pos)
        data_a.extend(data_neg)
        category = np.array(label_a+label_pos+label_neg, dtype=np.long).flatten()
        #blobs['data'] = data1
        #blobs['category'] = category
        blobs['data'] = torch.stack(data_a)
        blobs['category'] = torch.from_numpy(category)

    def get_blobs(self, images, cat_labels, training=True):

        blobs ={}
        self._add_triplet_data(images, cat_labels, blobs, training )

        inputs = {}
        inputs['data']=blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        return inputs, targets