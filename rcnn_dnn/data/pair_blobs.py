import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _find_scale(self, width, height, min_size, max_size):
            max_side = max(width, height)
            min_side = min(width, height)
            scale = float(min_size) / min_side
            if max_side * scale > max_size:
                scale = float(max_size) / max_side
            return scale

    def _cal_padding(self, images, size_devisable=32):
        max_size = tuple(max(s) for s in zip(*[image.shape for image in images]))
        stride = size_devisable

        max_size = list(max_size)
        max_size[0] = int(math.ceil(max_size[0] / stride) * stride)
        max_size[1] = int(math.ceil(max_size[1] / stride) * stride)

        for image in images:
            image.padding_y = int(max_size[0] - image.height)
            image.padding_x= int(max_size[1] - image.width)


    def _padding_batches(self, images):
        '''
        split the images to two groups as image pairs and 
        padding each group to same size
        '''
        images1 = []
        images2 = []
        for i, image in enumerate(images):
            if i%2 == 0:
                images1.append(image)
            else:
                images2.append(image)
        self._cal_padding(images1)
        self._cal_padding(images2)
        
    

    def _add_data( self, images, blobs, training ):
        data1 = []
        data2 = []
        data1_size = []
        data2_size = []

        min_size = self._dnn_cfg.MIN_SIZE
        max_size = self._dnn_cfg.MAX_SIZE

        # find scale for each image and padding for each image
        for i, image in enumerate(images) :
            #resize the image using scale in im_PIL
            width = image.width
            height = image.height
            scale = self._find_scale(width, height, min_size, max_size) 
            image.scale = scale

        # padding image to same size to form batches
        # padding is operated at right down side
        self._padding_batches(images)

        for i, image in enumerate(images) :
            im = image.im_PIL.convert('RGB') # the image scale and padding are excuted here

            pt_im = self._toTensor( im )
            pt_im = self._normalizeTensor( pt_im )

            if i%2 ==0:
                data1.append( pt_im )
                data1_size.append( image.ori_shape)
            else:
                data2.append( pt_im )
                data2_size.append( image.ori_shape)

        #blobs['data1'] = torch.stack(data1)
        #blobs['data2'] = torch.stack(data2)
        blobs['data1'] = ImageList(torch.stack(data1), data1_size)
        blobs['data2'] = ImageList(torch.stack(data2), data2_size)

    def _add_groundtruth(self, images, blobs, training):
        ground_truth = [[],[]]
        #gtbatches = [[],[]]

        for i,image in enumerate( images ) :
            boxes = image.gtboxes
            labels = image.gtlabels.reshape((-1))

            # Removing Small Boxes
            #boxes = self._remove_small_boxes( boxes, min_obj_side )
            #difficult = image.difficult

            if len( boxes ) > 0 :

                #if remove_difficults :
                #    inds = np.where( difficult == 1.0 )[0]
                #    labels[ inds ] = -1

                indices = np.ones((len(boxes),1)) * np.floor(i/2)

                ground_truth[i%2].append( {'boxes': torch.from_numpy(boxes), 'labels': torch.from_numpy(labels)} )


        for i in range(2):
            #if len( gtboxes[i] ) > 0 :
            #    gtboxes[i] = np.concatenate( gtboxes[i] )
            #    gtbatches[i] = np.concatenate( gtbatches[i] )
            #    gtlabels[i] = np.concatenate( gtlabels[i] )
            #else :
            #    gtboxes[i] = np.zeros([0,4])
            #    gtbatches[i] = np.zeros([0,1])
            #    gtlabels[i] = np.zeros([0,1])

            blobs['ground_truth{}'.format(i+1)] = ground_truth[i]
            #blobs['gtbatches{}'.format(i+1)] = gtbatches[i].astype( np.float32 )

    def _add_pair_labels(self, labels, blobs):
        pair_labels = labels.reshape((-1, 2))
        pair_labels = pair_labels[:,0]
        blobs['pair_labels'] = torch.from_numpy(pair_labels)

    def get_blobs( self, images, labels, training ):
        blobs = {}

        # Converting the data to the propper format
        self._add_data( images, blobs, training )
        self._add_groundtruth( images, blobs, training)
        self._add_pair_labels(labels, blobs)

        inputs = {}
        targets = {}
        for i in range(1,3):
            inputs['data{}'.format(i)] = blobs['data{}'.format(i)]

            targets['ground_truth{}'.format(i)] = blobs['ground_truth{}'.format(i)]
            #targets['gtlabels{}'.format(i)]= blobs['gtlabels{}'.format(i)]
        targets['pair_labels'] = blobs['pair_labels']

        return inputs, targets
