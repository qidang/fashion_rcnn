import numpy as np
import h5py
import torch
import random
import time
import datetime
from data import imageset


class imageset_retrieval(imageset) :
    def __init__( self, cfg, dataset, blob_gen, randomize=True, is_training=False, benchmark=None, random_sample_num = -1 ):
        self._cfg = cfg
        self._dataset = dataset
        self._blob_gen = blob_gen
        self._images = dataset.images
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        self._benchmark = benchmark
        self._random_sample_num = random_sample_num

        self._prepare()

    def _make_dicts(self):
        if hasattr(self, '_pair_dict') and hasattr(self, '_pair_ids') and hasattr(self,'_item_dict'):
            #print('pair_dicts was prepared')
            return
        self._pair_dict = {'shop':{}, 'user':{}}
        self._pair_ids = []
        self._item_dict = {'shop':[], 'user':[]}
        for ind, image in enumerate(self._images):
            source = image.source
            self._item_dict[source].append(ind)
            pair_id = image.pair_id
            if pair_id not in self._pair_ids:
                self._pair_ids.append(pair_id)
            if pair_id not in self._pair_dict[source]:
                self._pair_dict[source][pair_id]={}
            for style in image.styles:
                if style not in self._pair_dict[source][pair_id]:
                    self._pair_dict[source][pair_id][style] = []
                self._pair_dict[source][pair_id][style].append(ind)

    def _make_pos_pairs(self):
        if hasattr(self, '_pos_pairs'):
            #print('pos_pairs was prepared')
            return
        self._pos_pairs = []
        for pair_id in self._pair_ids:
            if pair_id not in self._pair_dict['user']:
                print('pair id {} is not available in user data'.format(pair_id))
                continue
            for style in self._pair_dict['shop'][pair_id]:
                if style == 0 or style not in self._pair_dict['user'][pair_id]:
                    continue
                shop_items = self._pair_dict['shop'][pair_id][style]
                user_items = self._pair_dict['user'][pair_id][style]
                for shop_item in shop_items:
                    for user_item in user_items:
                        self._pos_pairs.append([shop_item, user_item ])
    
    def _prepare(self):
        self._make_dicts()

        self._make_pos_pairs()

        self._pos_pairs = np.array(self._pos_pairs)

        npair = len(self._pos_pairs)
        if self._random_sample_num > 0 and self._random_sample_num < npair:
            ind = np.random.choice(npair, self._random_sample_num, replace=False)
            self._pairs = self._pos_pairs[ind,:]

        #print('shape of pairs is: ', self._pairs.shape)
        #self._pairs = np.array(self._pairs)
        if self._randomize:
            np.random.shuffle(self._pairs)
        #rand_pairs = np.random.choice(np.arange(npair), size=100)
        #for pair in rand_pairs:
        #    if ori_pairs[pair] not in self._pairs:
        #        print('something is wrong')
                
        self._labels = self._pairs[:,2:] # labels for positive/negative pairs
        self._pairs = self._pairs[:,:2]
        self._labels = self._labels.flatten()
        for i in range(int(len(self._labels)/2)):
            if self._labels[2*i] != self._labels[2*i+1]:
                print('pair labels have problems')
        self._pairs = self._pairs.flatten()

        inds = self._pairs
        ndata = len(inds)
        inds_label = np.arange(ndata)
        batchsize = self._batchsize
        
        #if self._randomize:
        #    np.random.shuffle(inds)
        
        self._chunks = [inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._label_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0


    def _fix_batches( self, roibatches ):
        roibatches = roibatches.ravel()

        for i in range(len(roibatches)) :
            roibatches[i] = i

        return roibatches.reshape((-1,1)).astype(np.int32)

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        images = self._images[self._chunks[idx]]
        labels = self._labels[self._label_chunks[idx]]
        blobs = self._blob_gen.get_blobs(images, labels, training=self._is_training)

        return blobs

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            # regenerate the input pairs
            self._prepare()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs
