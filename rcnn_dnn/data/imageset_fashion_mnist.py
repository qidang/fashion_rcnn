import os
import gzip
import numpy as np
from .mnist_blobs import mnist_blobs

import torchvision.transforms as transforms

class imageset_fashion_mnist:
    def __init__(self, cfg, randomize=False, is_training=True):
        self._randomize = randomize
        self._training = is_training
        self._root_path = os.path.join(cfg.dataset['ROOT'], 'datasets/fashion_mnist')
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE

        self._train_images, self._train_labels = self.load_mnist(self._root_path, kind='train')
        self._mean = self._train_images.mean()
        self._std = self._train_images.std()
        self._blob_gen = mnist_blobs(cfg)
        if is_training:
            self._images = self._train_images
            self._labels = self._train_labels
        else:
            self._images, self._labels = self.load_mnist(self._root_path, kind='t10k')
        self.init()
    
    def init(self):
        ndata = len(self._images)
        inds = np.arange(0, ndata)
        
        if self._randomize:
            np.random.shuffle(inds)
        self._chunks = [ inds[i:i+self._batchsize] for i in np.arange(0,ndata,self._batchsize) ]
        self._cur = 0
    
    def __len__(self):
        return len(self._chunks)

    def __getitem__(self, idx):
        ims = self._images[self._chunks[idx]].reshape((-1,1,28,28))
        labels = self._labels[self._chunks[idx]]
        inputs, targets = self._blob_gen.get_blobs(ims, labels)

        out = {}
        out['inputs']={}
        out['inputs'] = inputs

        out['targets'] = {}
        out['targets'] = targets

        return out['inputs'], out['targets']

    def next(self):
        if self._cur >= len(self):
            self._cur=0
        blobs = self[self._cur]
        self._cur = self._cur + 1
        return blobs

    def load_mnist(self, path, kind='train'):
        """Load MNIST data from `path`"""
        labels_path = os.path.join(path,
                                '%s-labels-idx1-ubyte.gz'
                                % kind)
        images_path = os.path.join(path,
                                '%s-images-idx3-ubyte.gz'
                                % kind)

        with gzip.open(labels_path, 'rb') as lbpath:
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                                offset=8)

        with gzip.open(images_path, 'rb') as imgpath:
            images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                                offset=16).reshape(len(labels), 784)

        return images, labels
