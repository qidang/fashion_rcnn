import numpy as np
import h5py
import torch
import itertools
import random
import datetime
import os

class imageset_fashion_hdf5 :
    def __init__( self, cfg, randomize=False, is_training=True, mode='user'):
        self._cfg = cfg
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._patch_size = self._cfg.dnn.PATCH_SIZE
        self._path = cfg.path.HDF5 % ( 'DeepFashion2_patch_{}'.format(self._patch_size) )
        if not os.path.isfile(self._path):
            raise ValueError('File {} not exist!'.format(self._path))
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        assert mode=='user' or mode=='shop'
        self._mode = mode
        if is_training:
            self._prefix = 'train'
        else:
            self._prefix = 'test'

        self.init()

    @property
    def file( self ):
        if self._file is None :
            self._file = h5py.File( self._path, 'r' )
        return self._file

    def init( self ):
        self._unique_id_max = self.file['{}_unique_id_max'.format(self._prefix)][0]
        self._make_id_list()
        self._get_valid_unique_ids()
        self._get_valid_item()

        self._shop_num = len(self._shop_list)
        self._user_num = len(self._user_list)

        if self._mode == 'user':
            ndata = self._user_num
            inds = self._user_list
        else:
            ndata = self._shop_num
            inds = self._shop_list

        if self._randomize:
            np.random.shuffle(inds)

        self._chunks = [ inds[i:i+self._batchsize] for i in np.arange(0,ndata,self._batchsize) ]
        self._cur = 0
        self._labels = self.file['{}_category'.format(self._prefix)][:] - 1
        self._uid = self.file['{}_unique_id'.format(self._prefix)][:]

    def _make_id_list(self):
        unique_ids = self.file['{}_unique_id'.format(self._prefix)]
        source = self.file['{}_source'.format(self._prefix)] # user: 0, shop: 1
        shop_id_list = []
        user_id_list = []
        for _ in range(self._unique_id_max+1):
            shop_id_list.append([])
            user_id_list.append([])
        for i, uni_id in enumerate(unique_ids):
            if source[i] == 0:
                user_id_list[uni_id].append(i)
            else:
                shop_id_list[uni_id].append(i)
        self._shop_id_list = shop_id_list
        self._user_id_list = user_id_list

    def _get_valid_unique_ids(self):
        self._valid_unique_ids = []
        for i in range(self._unique_id_max+1):
            #if self._shop_id_list[i] == [] or self._user_id_list[i] == []:
            #    continue
            # fileter out the ungood stuff
            if self._mode=='user' and len(self._shop_id_list[i]) < 1:
                continue
            self._valid_unique_ids.append(i)

    def _get_valid_item(self):
        self._shop_list = []
        self._user_list = []
        if self._mode == 'user':
            self._valid_unique_ids.remove(0)
        for uid in self._valid_unique_ids:
            self._shop_list.extend(self._shop_id_list[uid])
            self._user_list.extend(self._user_id_list[uid])

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        c = self._chunks[idx]
        # because the h5py only accept sorted index, so I sorted it and restored it later
        c_ordered = np.sort(c).tolist()
        c_ind = np.argsort(c)

        file = self.file

        out = {}
        out['inputs'] = {}
        out['inputs']['data'] = torch.from_numpy(file['{}_data'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        #out['inputs']['roibatches'] = self._fix_batches(file['roibatches'][c])

        out['targets'] = {}
        #out['targets']['category'] = torch.from_numpy(file['{}_category'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        out['targets']['category'] = torch.from_numpy(self._labels[c])
        #out['targets']['uid'] = torch.from_numpy(file['{}_unique_id'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        out['targets']['uid'] = torch.from_numpy(self._uid[c])
        #out['targets']['keypoints'] = file['keypoints'][c]

        return out['inputs'], out['targets']

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs