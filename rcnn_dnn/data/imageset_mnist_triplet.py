import os
import gzip
import numpy as np
from .mnist_blobs import mnist_blobs
from .mnist_pair_blobs import mnist_pair_blobs
from .mnist_triplet_blobs import mnist_triplet_blobs

import torchvision.transforms as transforms
import itertools
import random

class imageset_mnist_triplet:
    def __init__(self, cfg, randomize=False, is_training=True, sample_num=10000, hard_mining=False):
        self._randomize = randomize
        self._sample_num = sample_num
        self._training = is_training
        self._root_path = os.path.join(cfg.dataset['ROOT'], 'datasets/mnist')
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._hard_mining = hard_mining
        self._hard_triplets = None

        self._train_images, self._train_labels = self.load_mnist(self._root_path, kind='train')
        #self._mean = self._train_images.mean()
        #self._std = self._train_images.std()
        self._blob_gen = mnist_triplet_blobs(cfg)
        self._label_list_exist = False
        if is_training:
            self._images = self._train_images
            self._cat_labels = self._train_labels
        else:
            self._images, self._cat_labels = self.load_mnist(self._root_path, kind='t10k')
        self._prepare()
    
    def _prepare(self):
        self.make_label_lists()
        self.add_triplets(sample_num=self._sample_num)

        self._triplets = self._triplets.flatten()
                
        inds = self._triplets
        ndata = self._sample_num * 3
        batchsize = self._batchsize
        
        self._chunks = [inds[i:i+batchsize*3] for i in np.arange(0, ndata - batchsize*3, batchsize*3)]
        self._cur = 0

    def make_label_lists(self):
        if self._label_list_exist:
            return
        self._label_list = []
        for i in range(10):
            self._label_list.append([])
        for i, label in enumerate(self._cat_labels):
            self._label_list[label].append(i)
        self._label_list_exist = True


    def add_triplets(self, sample_num=10000):
        diff_pair_ind = list(itertools.combinations(range(10),2))
        self._triplets=[]
        for i in range(sample_num):
            ind = random.choice(diff_pair_ind)
            ind_anchor, ind_pos = random.sample(self._label_list[ind[0]], k=2 )
            ind_neg = random.choice(self._label_list[ind[1]])
            self._triplets.append([ind_anchor, ind_pos, ind_neg])
        self._triplets = np.array(self._triplets)

        # add hardming part
        if self._hard_mining and self._hard_triplets is not None:
            self._triplets = np.concatenate((self._triplets, self._hard_triplets), axis=0)
            np.random.shuffle(self._triplets)

    def add_hard_triplets(self, triplets_ind):
        self._hard_triplets = self._triplets.reshape(-1, 3)[triplets_ind]

    def __len__(self):
        return len(self._chunks)

    def __getitem__(self, idx):
        ims = self._images[self._chunks[idx]].reshape((-1,1,28,28))
        cat_labels = self._cat_labels[self._chunks[idx]]
        inputs, targets = self._blob_gen.get_blobs(ims, cat_labels)

        out = {}
        out['inputs']={}
        out['inputs'] = inputs

        out['targets'] = {}
        out['targets'] = targets

        return out['inputs'], out['targets']

    def next(self):
        if self._cur >= len(self):
            # select new data after each epoch
            self._prepare()
            #self._cur=0
        blobs = self[self._cur]
        self._cur = self._cur + 1
        return blobs

    def reset_cur(self):
        self._cur = 0

    def load_mnist(self, path, kind='train'):
        """Load MNIST data from `path`"""
        labels_path = os.path.join(path,
                                '%s-labels-idx1-ubyte.gz'
                                % kind)
        images_path = os.path.join(path,
                                '%s-images-idx3-ubyte.gz'
                                % kind)

        with gzip.open(labels_path, 'rb') as lbpath:
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                                offset=8)

        with gzip.open(images_path, 'rb') as imgpath:
            images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                                offset=16).reshape(len(labels), 784)

        return images, labels
