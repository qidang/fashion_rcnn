
import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class cifar10_pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=(0.5, 0.5, 0.5),
                                                     std=(0.5, 0.5, 0.5))
        self._randomCrop = transforms.RandomCrop(32, padding=4)
        self._randomHorizontalFlip = transforms.RandomHorizontalFlip()

    def _im_to_tensor(self, im, training):
        im_new = Image.fromarray(im.reshape((3, 32, 32)).transpose([1,2,0]), mode='RGB')

        #im = torch.from_numpy(im)
        if training:
            im_new = self._randomCrop(im_new)
            im_new = self._randomHorizontalFlip(im_new)
        im = self._toTensor(im_new)
        im = self._normalizeTensor(im)
        return im


    def _add_pair_data(self, images, cat_labels, training, blobs):
        data1 = []
        data2 = []

        label1 = []
        label2 = []
        #for image in images:
        #    print('pair id is: ',image.pair_id)

        pair_num = int(len(images) / 2)
        for i in range(pair_num):
            image1 = images[2*i]
            image2 = images[2*i+1]

            image1 = self._im_to_tensor(image1, training)
            image2 = self._im_to_tensor(image2, training)
            
            category1 = cat_labels[2*i]
            category2 = cat_labels[2*i+1]


            data1.append(image1)
            data2.append(image2)

            label1.append(category1)
            label2.append(category2)
        data1.extend(data2)
        category = np.array(label1+label2, dtype=np.long).flatten()
        #blobs['data'] = data1
        #blobs['category'] = category
        blobs['data'] = torch.stack(data1)
        blobs['category'] = torch.from_numpy(category)

    def _add_pair_labels(self, labels, blobs):
        blobs['pair_labels'] = torch.from_numpy(labels)

    def get_blobs(self, images, pair_labels, cat_labels, training=True):
        pair_labels = pair_labels.reshape((-1, 2))
        pair_labels = pair_labels[:,0]

        blobs ={}
        self._add_pair_data(images, cat_labels, training, blobs)
        self._add_pair_labels(pair_labels, blobs)

        inputs = {}
        inputs['data']=blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        targets['pair_labels'] = blobs['pair_labels']
        return inputs, targets