import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms
import random

class patch_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_data( self, images, blobs, training ):
        data = []

        im_size = self._dnn_cfg.PATCH_SIZE

        for image in images :
            im = image.im_PIL.convert('RGB')
            for box in image.boxes:

                box = np.floor( box ).astype( int )

                # Extracting the patch
                patch = im.crop((box[0],box[1],box[2],box[3]))
                patch = patch.resize((im_size, im_size))

                pt_patch = self._toTensor( patch )
                pt_patch = self._normalizeTensor( pt_patch )

                data.append( pt_patch )

        blobs['data'] = torch.stack( data )

    def _add_categories( self, images, blobs, training ):
        categories = []

        for image in images :
            c = image.categories.ravel().astype(int)
            categories.append( c )

        gtcategory = np.concatenate( categories )
        blobs['category'] = torch.from_numpy( gtcategory )

    def _add_unique_id(self, images, blobs):
        unique_ids = []
        for image in images:
            unique_id = image.unique_id
            unique_ids.append(unique_id)
        unique_ids = np.concatenate(unique_ids)
        blobs['unique_ids'] = torch.from_numpy(unique_ids)


    def get_blobs( self, images, training=True ):

        blobs = {}
        self._add_data(images, blobs, training)
        self._add_categories(images, blobs, training)
        self._add_unique_id(images, blobs)

        inputs = {}
        inputs['data'] = blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        targets['unique_ids'] = blobs['unique_ids']
        return inputs, targets
