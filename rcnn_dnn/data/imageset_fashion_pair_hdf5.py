import numpy as np
import h5py
import torch
import itertools
import random
import datetime
import os

class imageset_fashion_pair_hdf5 :
    def __init__( self, cfg, randomize=False, is_training=True, sample_num_pos=10000, random_mirror=False ):
        self._cfg = cfg
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._patch_size = self._cfg.dnn.PATCH_SIZE
        self._path = cfg.path.HDF5 % ( 'DeepFashion2_patch_{}'.format(self._patch_size) )
        if not os.path.isfile(self._path):
            raise ValueError('File {} not exist!'.format(self._path))
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        self._sample_num_pos = sample_num_pos
        self._random_mirror = random_mirror
        if is_training:
            self._prefix = 'train'
        else:
            self._prefix = 'test'

        self.init()

    @property
    def file( self ):
        if self._file is None :
            self._file = h5py.File( self._path, 'r' )
        return self._file

    def init( self ):
        self._unique_id_max = self.file['{}_unique_id_max'.format(self._prefix)][0]
        self._make_id_list()
        self._get_valid_unique_ids()
        self._get_pairs()
        ndata = len(self._pairs)

        inds = self._pairs
        label_inds = np.arange(ndata)

        self._chunks = [ inds[i:i+self._batchsize] for i in np.arange(0,ndata,self._batchsize) ]
        self._label_chunks = [label_inds[i:i+self._batchsize] for i in np.arange(0, ndata, self._batchsize)]
        self._cur = 0
        self._cat_labels = self.file['{}_category'.format(self._prefix)][:] - 1
        self._uid = self.file['{}_unique_id'.format(self._prefix)][:]

    def re_init(self):
        self._get_pairs()
        ndata = len(self._pairs)

        inds = self._pairs
        label_inds = np.arange(ndata)

        self._chunks = [ inds[i:i+self._batchsize] for i in np.arange(0,ndata,self._batchsize) ]
        self._label_chunks = [label_inds[i:i+self._batchsize] for i in np.arange(0, ndata, self._batchsize)]
        self._cur = 0

    def _make_id_list(self):
        unique_ids = self.file['{}_unique_id'.format(self._prefix)]
        source = self.file['{}_source'.format(self._prefix)] # user: 0, shop: 1
        shop_id_list = []
        user_id_list = []
        for _ in range(self._unique_id_max):
            shop_id_list.append([])
            user_id_list.append([])
        for i, uni_id in enumerate(unique_ids):
            if source[i] == 0:
                user_id_list[uni_id-1].append(i)
            else:
                shop_id_list[uni_id-1].append(i)
        self._shop_id_list = shop_id_list
        self._user_id_list = user_id_list

    def _get_valid_unique_ids(self):
        self._valid_unique_ids = []
        for i in range(self._unique_id_max):
            if self._shop_id_list[i] == [] or self._user_id_list[i] == []:
                continue
            self._valid_unique_ids.append(i)

    def _get_pairs(self):
        self._get_pos_samples()
        self._get_neg_samples()
        self._pairs_with_label = np.concatenate((self._pos_pairs, self._neg_pairs), axis=0)
        #self._pairs = self._pos_pairs
        np.random.shuffle(self._pairs_with_label)
        self._labels = self._pairs_with_label[:,2:].flatten()
        self._pairs = self._pairs_with_label[:,:2].flatten()

    def _get_pos_samples(self):
        i=0
        pairs_with_label = np.ones((self._sample_num_pos, 4), dtype=int)
        while i < self._sample_num_pos:
            uid = random.choice(self._valid_unique_ids)
            pos_user = random.choice(self._user_id_list[uid])
            pos_shop = random.choice(self._shop_id_list[uid])
            pairs_with_label[i][0] = pos_user
            pairs_with_label[i][1] = pos_shop
            i += 1
        self._pos_pairs = pairs_with_label

    def _get_neg_samples(self, ratio=1):
        i=0
        neg_num = self._sample_num_pos * ratio
        pairs_with_label = np.zeros((neg_num, 4), dtype=int)
        while i < neg_num:
            uid1, uid2 = random.sample(self._valid_unique_ids, 2)
            pos_user = random.choice(self._user_id_list[uid1])
            pos_shop = random.choice(self._shop_id_list[uid2])
            pairs_with_label[i][0] = pos_user
            pairs_with_label[i][1] = pos_shop
            i += 1
        self._neg_pairs = pairs_with_label

    def __len__( self ):
        return len(self._chunks)

    def random_flip(self, data):
        for i, im in enumerate(data):
            if random.random()<0.5: # 50% true and 50% false
                data[i] = np.flip(im, axis=1)
        return data
        

    def __getitem__( self, idx ):
        c = self._chunks[idx]
        c_label = self._label_chunks[idx]
        #print(c_label)
        c = c.reshape((-1,2)).transpose().flatten()
        #c_label = c_label.reshape((-1,2)).transpose().flatten()
        # because the h5py only accept sorted index, so I sorted it and restored it later
        c_ordered, ind_rev = np.unique(c, return_inverse=True)
        c_ordered = c_ordered.tolist()
        #c_ordered = np.sort(c).tolist()
        #c_ind = np.argsort(c)
        c_ind = ind_rev

        file = self.file

        out = {}
        out['inputs'] = {}
        numpy_data = file['{}_data'.format(self._prefix)][c_ordered][c_ind]
        if self._random_mirror:
            numpy_data = self.random_flip(numpy_data)
        out['inputs']['data'] = torch.from_numpy(numpy_data)
        #out['inputs']['roibatches'] = self._fix_batches(file['roibatches'][c])

        out['targets'] = {}
        #out['targets']['category'] = torch.from_numpy(file['{}_category'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        out['targets']['category'] = torch.from_numpy(self._cat_labels[c])
        out['targets']['pair_labels'] = torch.from_numpy(self._labels[c_label].reshape((-1,2))[:,0])
        #out['targets']['pair_labels1'] = torch.from_numpy(self._labels[c_label].reshape((-1,2))[:,1])
        #out['targets']['uid'] = torch.from_numpy(file['{}_unique_id'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        #out['targets']['uid_correct'] = torch.from_numpy(self._uid[c])
        #out['targets']['keypoints'] = file['keypoints'][c]

        return out['inputs'], out['targets']

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            self.re_init()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs