import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from PIL import Image
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms
import random

class mnist_pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_pair_data(self, images, cat_labels, blobs):
        data1 = []
        data2 = []

        label1 = []
        label2 = []
        #for image in images:
        #    print('pair id is: ',image.pair_id)

        pair_num = int(len(images) / 2)
        for i in range(pair_num):
            image1 = images[2*i]
            image2 = images[2*i+1]

            image1 = image1.astype(np.float32)/255.0
            image1 = torch.from_numpy(image1)

            image2 = image2.astype(np.float32)/255.0
            image2 = torch.from_numpy(image2)
            
            category1 = cat_labels[2*i]
            category2 = cat_labels[2*i+1]


            data1.append(image1)
            data2.append(image2)

            label1.append(category1)
            label2.append(category2)
        data1.extend(data2)
        category = np.array(label1+label2, dtype=np.long).flatten()
        #blobs['data'] = data1
        #blobs['category'] = category
        blobs['data'] = torch.stack(data1)
        blobs['category'] = torch.from_numpy(category)
                    
    def _add_pair_labels(self, labels, blobs):
        blobs['pair_labels'] = torch.from_numpy(labels)

    def get_blobs( self, images, pair_labels, cat_labels):
        assert len(images)==len(pair_labels)
        pair_labels = pair_labels.reshape((-1, 2))
        pair_labels = pair_labels[:,0]

        blobs = {}
        self._add_pair_data(images, cat_labels, blobs)
        self._add_pair_labels(pair_labels, blobs)

        inputs = {}
        inputs['data'] = blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        targets['pair_labels'] = blobs['pair_labels']
        return inputs, targets
