import numpy as np
import h5py
import torch
import itertools
import random
import datetime

class imageset_fashion_triplet_hdf5 :
    def __init__( self, cfg, randomize=False, is_training=True, random_sample_num=10000 ):
        self._cfg = cfg
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._path = cfg.path.HDF5 % ( 'DeepFashion2_patch' )
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        self._sample_num = random_sample_num
        if is_training:
            self._prefix = 'train'
        else:
            self._prefix = 'test'

        self.init()

    @property
    def file( self ):
        if self._file is None :
            self._file = h5py.File( self._path, 'r' )
        return self._file

    def init( self ):
        self._unique_id_max = self.file['{}_unique_id_max'.format(self._prefix)][0]
        self._make_id_list()
        self._get_valid_unique_ids()
        self._get_samples()
        ndata = self._sample_num * 3

        inds = self._triplets

        self._chunks = [ inds[i:i+self._batchsize*3] for i in np.arange(0,ndata,self._batchsize*3) ]
        self._cur = 0
        self._labels = self.file['{}_category'.format(self._prefix)][:]
        self._uid = self.file['{}_unique_id'.format(self._prefix)][:]

    def re_init(self):
        self._get_samples()
        ndata = self._sample_num * 3

        inds = self._triplets

        self._chunks = [ inds[i:i+self._batchsize*3] for i in np.arange(0,ndata,self._batchsize*3) ]
        self._cur = 0

    def _make_id_list(self):
        unique_ids = self.file['{}_unique_id'.format(self._prefix)]
        source = self.file['{}_source'.format(self._prefix)] # user: 0, shop: 1
        shop_id_list = []
        user_id_list = []
        for _ in range(self._unique_id_max):
            shop_id_list.append([])
            user_id_list.append([])
        for i, uni_id in enumerate(unique_ids):
            if source[i] == 0:
                user_id_list[uni_id-1].append(i)
            else:
                shop_id_list[uni_id-1].append(i)
        self._shop_id_list = shop_id_list
        self._user_id_list = user_id_list

    def _get_valid_unique_ids(self):
        self._valid_unique_ids = []
        for i in range(self._unique_id_max):
            if self._shop_id_list[i] == [] or self._user_id_list[i] == []:
                continue
            self._valid_unique_ids.append(i)

    def _get_samples(self):
        i=0
        triplets = np.zeros((self._sample_num, 3), dtype=int)
        sample_frequency = self._batchsize * 2
        while i < self._sample_num:
            if i % sample_frequency == 0:
                inds = random.sample(self._valid_unique_ids, sample_frequency)
            i1 = inds[(i%self._batchsize)*2]
            i2 = inds[(i%self._batchsize)*2+1]
            # TODO: delete the following after test
            if self._shop_id_list[i1] == []:
                print('shop item {} is empty'.format(i1))
                continue
            if self._shop_id_list[i2] == []:
                print('shop item {} is empty'.format(i2))
                continue
            if self._user_id_list[i1] == []:
                print('user item {} is empty'.format(i2))
                continue
            anchor = random.choice(self._user_id_list[i1])
            pos = random.choice(self._shop_id_list[i1])
            neg = random.choice(self._shop_id_list[i2])
            triplets[i][0] = anchor
            triplets[i][1] = pos
            triplets[i][2] = neg
            i += 1
        self._triplets = triplets.flatten()

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        c = self._chunks[idx]
        c = c.reshape((-1,3)).transpose().flatten()
        # because the h5py only accept sorted index, so I sorted it and restored it later
        c_ordered = np.sort(c).tolist()
        c_ind = np.argsort(c)

        file = self.file

        out = {}
        out['inputs'] = {}
        out['inputs']['data'] = torch.from_numpy(file['{}_data'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        #out['inputs']['roibatches'] = self._fix_batches(file['roibatches'][c])

        out['targets'] = {}
        #out['targets']['category'] = torch.from_numpy(file['{}_category'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        out['targets']['category'] = torch.from_numpy(self._labels[c])
        #out['targets']['uid'] = torch.from_numpy(file['{}_unique_id'.format(self._prefix)][c_ordered][np.argsort(c_ind)])
        #out['targets']['uid_correct'] = torch.from_numpy(self._uid[c])
        #out['targets']['keypoints'] = file['keypoints'][c]

        return out['inputs'], out['targets']

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            self.re_init()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs