import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common

#from dnn.extensions.modules import RoiAlign
from roi_align.roi_align import RoIAlign

class Net( nn.Module ):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg

        self.feature = networks.feature['vgg16']( feature=True )
        self.pooling = RoIAlign(7,7,transform_fpcoor=False)

        self._child_networks = ['feature','pooling']

        # Fully connected layers
        self.fc1 = nn.Linear( 25088, 4096 )
        self.fc2 = nn.Linear( 4096, 4096 )
        self.fc3 = nn.Linear( 4096, self._cfg.dnn.NETWORK.NCLASSES )

        self._init_weights()

    def forward( self, inputs ):
        # VGG16
        x = self.feature(inputs['data'])

        # RoiAlign
        x = self.pooling(x,inputs['gtboxes'], inputs['gtbatches'])
        #x = F.max_pool2d(x,2)

        # Fully Connected
        x = x.view(-1, 25088)

        x = F.relu(self.fc1(x))
        x = F.dropout(x,training=self.training)
        x = F.relu(self.fc2(x))
        x = F.dropout(x,training=self.training)
        x = self.fc3(x)
        return x

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.criterion = nn.CrossEntropyLoss()

    def __call__( self, outputs, targets ):
        return self.criterion( outputs, targets['gtcategory'] )

class Benchmark :
    def __init__( self ):
        self._total = 0
        self._correct = 0

    def update( self, targets, pred ):
        cat = targets['gtcategory'].detach().numpy()
        pred = pred.detach().numpy()
        cls = np.argmax( pred, axis=1 ).ravel()

        total = len(cls)
        correct = len(np.where( cat == cls )[0])

        self._total = self._total + total
        self._correct = self._correct + correct

    def summary( self ):
        print('Validation Accuracy : %f' % ( float(self._correct)/self._total) )
