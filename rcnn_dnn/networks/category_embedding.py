import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common

class EmbeddingNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg

        self.feature = networks.feature['vgg16']( feature=False )
        self._child_networks = ['feature']

        self.fc1 = nn.Conv2d(512, 2048, kernel_size=2)
        self.fc2 = nn.Conv2d(2048, 2048, kernel_size=1)
        self._init_weights()

    def forward(self, inputs):
        # VGG16
        x = self.feature(inputs['data'])

        x = F.relu(self.fc1(x))
        #x_anchor = F.dropout(x_anchor,training=self.training)
        x= F.relu(self.fc2(x))

        output = x
        return output


class Net( nn.Module ):
    def __init__( self, cfg):
        super().__init__()
        self._cfg = cfg
        self._embedding_net = EmbeddingNet(cfg)

    def forward( self, inputs ):
        # VGG16
        x_anchor = self._embedding_net({'data':inputs['anchor_data']})
        x_pos = self._embedding_net({'data':inputs['pos_data']})
        x_neg = self._embedding_net({'data':inputs['neg_data']})

        outputs = {}
        outputs['anchor'] = x_anchor
        outputs['pos'] = x_pos
        outputs['neg'] = x_neg
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.criterion = nn.TripletMarginLoss(margin=0.6)

    def __call__( self, outputs, targets):
        anchor = outputs['anchor']
        pos = outputs['pos']
        neg = outputs['neg']
        return self.criterion( anchor, pos, neg)

class Benchmark :
    def __init__( self ):
        self._total = 0
        self._correct = 0

    def update( self, targets, pred ):
        cat = targets['gtcategory'].detach().numpy()
        pred = pred.detach().numpy()
        cls = np.argmax( pred, axis=1 ).ravel()

        total = len(cls)
        correct = len(np.where( cat == cls )[0])

        self._total = self._total + total
        self._correct = self._correct + correct

    def summary( self ):
        print('Validation Accuracy : %f' % ( float(self._correct)/self._total) )
