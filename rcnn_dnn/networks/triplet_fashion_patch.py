import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
from tools import Logger
import datetime
from torchvision.models import vgg16_bn, resnet50

class TripletNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self._batch_size = cfg.dnn.NETWORK.BATCH_SIZE
        self._feature_dim = cfg.dnn.NETWORK.FEATURE_DIM

        #self.feature = networks.feature['EasyNetD']( feature=False, RGB=True )
        # resnet feature
        resnet = resnet50(pretrained=True)
        modules = list(resnet.children())[:-1]      # delete the last fc layer.
        resnet = nn.Sequential(*modules)
        self.feature = resnet
        #self.feature = vgg16_bn(pretrained=True).features
        self._child_networks = ['feature']

        #self.dropout = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(2048, 10)
        self.fc_embedding = nn.Linear(2048, self._feature_dim)

        # Match Net
        self._init_weights()

    def forward(self, inputs, just_embedding=False):
        x_f = self.feature(inputs['data'])

        # match

        x_f = x_f.view(-1, 2048)
        x_embedding = self.fc_embedding(x_f)

        if just_embedding:
            return {'embeddings': x_embedding}
        x1 = x_embedding[:self._batch_size, :]
        x2 = x_embedding[self._batch_size:2*self._batch_size, :]
        x3 = x_embedding[self._batch_size*2:,:]
        #diff = torch.pow(x2-x1, 2)
        #category
        #x = x_f.view(-1, 2048)
        #x = F.relu(self.fc1(x))
        x= self.fc1(x_f)

        outputs = {}
        outputs['category'] = x
        outputs['anchor'] = x1
        outputs['positive'] = x2
        outputs['negative'] = x3
        #outputs['match'] = match_out
        outputs['embeddings'] = x_embedding
        #outputs['l1'] = l1
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.cat_criterion = nn.CrossEntropyLoss()
        self.match_criterion = nn.TripletMarginLoss(margin=1.0, p=2)

    def __call__( self, outputs, targets):
        losses = {}
        #losses['category'] = self.cat_criterion(outputs['category'], targets['category'])
        losses['match'] = self.match_criterion(outputs['anchor'], outputs['positive'], outputs['negative'])
        return losses
