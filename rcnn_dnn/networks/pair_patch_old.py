import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
import logging

class Net( nn.Module ):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self._batch_size = cfg.dnn['NETWORK']['BATCH_SIZE']
        self._half_batch = int(self._batch_size / 2)
        assert self._batch_size%2 == 0

        self.feature = networks.feature['vgg16']( feature=False, pretrained=True )
        self._child_networks = ['feature']

        # Fully connected layers
        self.fc1 = nn.Linear( 2048, 4096 )
        self.fc2 = nn.Linear( 4096, 4096 )
        self.fc3 = nn.Linear( 4096, self._cfg.dnn.NETWORK.NCLASSES )

        # layers for similarity
        self.mn_conv1 = nn.Conv2d(512, 256, 1)
        self.mn_conv2 = nn.Conv2d(256, 1024, 1)
        self.mn_fc1 = nn.Linear(4096, 1024)
        self.mn_fc2 = nn.Linear(1024, 256)
        self.mn_fc3 = nn.Linear(256, 2)

        self._init_weights()

    def forward( self, inputs ):
        # VGG16
        x = self.feature(inputs['data'])

        #similarity
        x1 = x[:self._half_batch,:,:,:]
        x2 = x[-self._half_batch:,:,:,:]

        x1 = F.relu(self.mn_conv1(x1))
        x2 = F.relu(self.mn_conv1(x2))

        x1 = F.relu(self.mn_conv2(x1))
        x2 = F.relu(self.mn_conv2(x2))

        x1 = x1.view(-1, 4096)
        x2 = x2.view(-1, 4096)

        x1 = F.relu(self.mn_fc1(x1))
        x2 = F.relu(self.mn_fc1(x2))
        x1 = F.relu(self.mn_fc2(x1))
        x2 = F.relu(self.mn_fc2(x2))

        diff = torch.pow(x2-x1, 2)
        match_out = self.mn_fc3(diff)

        # Fully Connected for classification
        x = x.view(-1, 2048)

        x = F.relu(self.fc1(x))
        x = F.dropout(x,training=self.training)
        x = F.relu(self.fc2(x))
        x = F.dropout(x,training=self.training)
        category_out = self.fc3(x)

        outputs = {}
        outputs['match'] = match_out
        outputs['category'] = category_out
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.criterion_category = nn.CrossEntropyLoss()
        self.criterion_match = nn.CrossEntropyLoss()

    def __call__( self, outputs, targets ):
        loss = {}
        loss['category'] = self.criterion_category( outputs['category'], targets['category'] )
        loss['match'] = self.criterion_match( outputs['match'], targets['pair_labels'] )

        return loss
class Benchmark :
    def __init__( self, cfg, criterias=['match'] ):
        self._criterias = criterias
        self._cfg = cfg
        if 'match' in criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in criterias:
            self._total = 0
            self._correct = 0
        self.init_log()

    def init_log(self):
        bench_path = self._cfg.path['BENCHMARK_LOG']
        self._bench_logger = logging.getLogger('benchmark')

        c_handler = logging.StreamHandler()
        f_handler = logging.FileHandler(bench_path)

        c_handler.setLevel(logging.INFO)
        f_handler.setLevel(logging.INFO)

        c_formatter = logging.Formatter('%(asctime)s %(message)s')
        f_formatter = logging.Formatter('%(asctime)s %(message)s')
        c_handler.setFormatter(c_formatter)
        f_handler.setFormatter(f_formatter)

        self._bench_logger.addHandler(c_handler)
        self._bench_logger.addHandler(f_handler)

    def update( self, targets, pred ):
        if 'match' in self._criterias:
            match_label = targets['pair_labels'].detach().numpy()
            pre = pred['match'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()
            total = len(cls)
            correct = len(np.where(match_label == cls)[0])
            self._match_total += total
            self._match_correct += correct

        if 'category' in self._criterias:
            cat = targets['category'].detach().numpy()
            pre = pred['category'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()

            total = len(cls)
            correct = len(np.where( cat == cls )[0])

            self._total = self._total + total
            self._correct = self._correct + correct

    def summary( self ):
        if 'match' in self._criterias:
            self._bench_logger.info('Match Accuracy : {}'.format(float(self._match_correct)/ self._match_total))
            print('Match Accuracy : {}'.format(float(self._match_correct)/ self._match_total))
        if 'category' in self._criterias:
            self._bench_logger.info('Category Validation Accuracy : %f' % ( float(self._correct)/self._total) )
            print('Category Validation Accuracy : %f' % ( float(self._correct)/self._total))

        if 'match' in self._criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in self._criterias:
            self._total = 0
            self._correct = 0
