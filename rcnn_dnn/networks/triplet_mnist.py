import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
#from dnn.networks.tools import Regularization
from tools import Logger
import datetime


class TripletEasyNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self._batch_size = cfg.dnn.NETWORK.BATCH_SIZE
        self._pair_batch_size = int(cfg.dnn.NETWORK.BATCH_SIZE / 2)

        self.feature = networks.feature['EasyNetD']( feature=False, RGB=False )
        self._child_networks = ['feature']

        self.dropout = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(7*7*128, 500)
        #self.fc2 = nn.Linear(500, 10)

        self._init_weights()

        #self.reg_loss = Regularization(self, weight_decay=0.1, p=1)


    def forward(self, inputs):
        x = self.feature(inputs['data'])

        x = x.view(-1, 128*7*7)
        x = self.fc1(x)

        # match
        x1 = x[:self._batch_size, :]
        x2 = x[self._batch_size:2*self._batch_size, :]
        x3 = x[self._batch_size*2:,:]

        outputs = {}
        outputs['anchor'] = x1
        outputs['positive'] = x2
        outputs['negative'] = x3

        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.cat_criterion = nn.CrossEntropyLoss()
        self.match_criterion = nn.TripletMarginLoss(margin=1.0, p=2)

    def __call__( self, outputs, targets):
        losses = {}
        #losses['category'] = self.cat_criterion(outputs['category'], targets['category'])
        losses['match'] = self.match_criterion(outputs['anchor'], outputs['positive'], outputs['negative'])
        return losses

class Benchmark :
    def __init__( self, cfg, criterias=['category'] ):
        self._criterias = criterias
        self._cfg = cfg
        if 'match' in criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in criterias:
            self._total = 0
            self._correct = 0
        self.init_logger()

    def init_logger(self):
        bench_path = self._cfg.path['BENCHMARK_LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=bench_path, console=False, console_formatter=console_formatter)

        #self._logger.info('benchmark log path is: {}'.format(bench_path))
        print('The benchmark log path is {}'.format(bench_path))

    def update( self, targets, pred ):
        if 'category' in self._criterias:
            cat = targets['category'].detach().numpy()
            pre = pred['category'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()

            total = len(cls)
            correct = len(np.where( cat == cls )[0])

            self._total = self._total + total
            self._correct = self._correct + correct
        if 'match' in self._criterias:
            match_label = targets['pair_labels'].detach().numpy()
            pre = pred['match'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()
            total = len(cls)
            correct = len(np.where(match_label == cls)[0])
            self._match_total += total
            self._match_correct += correct

    def summary( self ):
        if 'match' in self._criterias:
            accuracy = float(self._match_correct)/ self._match_total
            self._logger.info('{} '.format(accuracy))
            print('Match Accuracy : {}'.format(accuracy))
        if 'category' in self._criterias:
            accuracy = float(self._correct)/self._total
            self._logger.info('{} '.format(accuracy))
            print('Category Accuracy : {}'.format(accuracy) )

        if 'match' in self._criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in self._criterias:
            self._total = 0
            self._correct = 0

class Match_Head(nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            #if len(name)>0 and name.split('.')[0] not in self._child_networks :
            networks_common.init(m)

    def __init__(self):
        super().__init__()
        self.mn_conv1 = nn.Conv2d(128, 256, 1)
        self.mn_conv2 = nn.Conv2d(256, 256, 1)
        self.mn_fc1 = nn.Linear(7*7*256, 256)
        self.mn_fc2 = nn.Linear(256, 256)
        self._init_weights()
        #self.mn_fc = nn.Linear(7*7*128,256)

    def forward(self, x):
        x=F.relu(self.mn_conv1(x))
        x=F.relu(self.mn_conv2(x))
        x = x.view(-1, 7*7*256)

        x = F.relu(self.mn_fc1(x))
        x = F.relu(self.mn_fc2(x))
        #x = x.view(-1, 7*7*128)
        #x = F.relu(self.mn_fc(x))
        return x
