from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'cifar10'
    cfg.DATASET.WORKSET_SETTING = []

    cfg.DNN['train'].NITER = 100
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.05,'momentum':0.9, 'weight_decay':5e-4, 'decay_step':50,'decay_rate':0.5}
    #cfg.DNN['train'].OPTIMIZER = {'type':'Adam','lr':0.01,'decay_step':5,'decay_rate':0.1}

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg