from . import base as baseline
from . import conf_proposal
from . import rcnn_vgg16
from . import pair_patch_vgg16
from . import mnist
from .import cifar10
from . import mnist_pair
from . import mnist_triplet
from . import cifar10_pair
from . import cifar10_triplet
from . import triplet_patch_resnet
from . import pair_patch_resnet
from . import pair_roi_resnet
from . import patch_pair_roi_resnet


configurations = {}

configurations['baseline'] = baseline
configurations['conf_proposal'] = conf_proposal
configurations['rcnn_vgg16'] = rcnn_vgg16
configurations['pair_patch_vgg16'] = pair_patch_vgg16
configurations['mnist'] = mnist
configurations['mnist_pair'] = mnist_pair
configurations['mnist_triplet'] = mnist_triplet
configurations['cifar10'] = cifar10
configurations['cifar10_pair'] = cifar10_pair
configurations['cifar10_triplet'] = cifar10_triplet
configurations['triplet_patch_resnet'] = triplet_patch_resnet
configurations['pair_patch_resnet'] = pair_patch_resnet
configurations['pair_roi_resnet'] = pair_roi_resnet
configurations['patch_pair_roi_resnet'] = patch_pair_roi_resnet
