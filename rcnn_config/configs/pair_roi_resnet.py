from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'pair_roi_resnet'
    cfg.DATASET.WORKSET_SETTING = []

    cfg.DNN['train'].MIN_SIZE = 800
    cfg.DNN['test'].MIN_SIZE = 800

    cfg.DNN['train'].MAX_SIZE = 800
    cfg.DNN['test'].MAX_SIZE = 800

    #cfg.DNN['train'].MIN_SIZE = 400
    #cfg.DNN['test'].MIN_SIZE = 400

    #cfg.DNN['train'].MAX_SIZE = 600
    #cfg.DNN['test'].MAX_SIZE = 600

    cfg.DNN['train'].NITER = 35
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.0002,'decay_step':8,'decay_rate':0.1}

    cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg