import _init
import os
import argparse
import pickle
import platform

#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data import imageset_retrieval, imageset_fashion_mnist, imageset_cifar10, imageset_mnist_pair, imageset_cifar10_pair, imageset_mnist_triplet, imageset_mnist_kgen, imageset_fashion_triplet_hdf5,imageset_fashion_pair_hdf5, imageset_fashion_roi_pair, imageset_fashion_roi, imageset_fashion_patch_roi_pair, imageset_fashion_patch_roi, imageset_fashion_hdf5
from rcnn_dnn.data import pair_blobs, patch_pair_blobs, roi_pair_blobs, roi_blobs, patch_roi_pair_blobs, patch_roi_blobs
from rcnn_dnn.networks import networks

import torch
import torchvision.transforms as transforms

from dnn import trainer
import numpy as np

def get_dataset():
    params = {}
    params['config'] = 'pair_patch_resnet'
    params['batch_size'] = 256
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20190909_pair'
    params['path'] = {}

    #criterias = ['match']
    #criterias = ['category_accuracy', 'pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['top_k_retrieval_accuracy']
    #criterias = ['category_accuracy']
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )


    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    #test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='vgg16' )

    #benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None

    #blobs_gen = patch_pair_blobs(cfg.dnn)
    #test_imageset = imageset_fashion_triplet_hdf5(cfg, is_training=False, randomize=False, random_sample_num=5120 )
    #train_imageset = imageset_fashion_triplet_hdf5(cfg, is_training=True, randomize=True, random_sample_num=102400)
    #train_imageset = imageset_fashion_pair_hdf5(cfg, is_training=True, randomize=True, sample_num_pos=102400)
    #test_imageset = imageset_fashion_pair_hdf5(cfg, is_training=True, randomize=True, sample_num_pos=10240)
    test_imageset_shop = imageset_fashion_hdf5(cfg, is_training=False, mode='shop')
    #test_imageset_user = imageset_fashion_hdf5(cfg, is_training=False, mode='user')
    return cfg, test_imageset_shop

def get_fashion_dataset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'patch_pair_roi_resnet'
    params['nclasses'] = 13
    params['batch_size'] = 4
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    blob_gen = patch_roi_pair_blobs(cfg.dnn)
    #blob_gen = patch_roi_blobs(cfg.dnn)
    #test_blob_gen = roi_blobs(cfg.dnn)

    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )
    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='resnet50' )
    train_set = imageset_fashion_patch_roi_pair(cfg, test_dataset, blob_gen, randomize=True, is_training=True, sample_num_pos= 10240, random_mirror=True)
    #train_set = imageset_fashion_patch_roi(cfg, test_dataset, blob_gen, mode='shop', randomize=False )
    #train_set = imageset_fashion_roi(cfg, train_dataset, blob_gen, randomize=True, is_training=True )
    #test_set = imageset_fashion_roi(cfg, test_dataset, test_blob_gen, is_training=False, mode='shop')
    return cfg, train_set

def get_mnist_triplet_imageset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'mnist_triplet'
    params['nclasses'] = 10
    params['batch_size'] = 16
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    imageset = imageset_mnist_triplet(cfg, randomize=True, is_training=True, sample_num=10000)
    #cfg.build_path( params['tag'], 'mnist', model_hash='vgg16' )
    return cfg, imageset
def get_mnist_imageset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'pair_patch_vgg16'
    params['nclasses'] = 10
    params['batch_size'] = 16
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    k_list=[5, 10, 20]
    imageset = imageset_mnist_kgen(cfg, k_list, is_training=True)
    #cfg.build_path( params['tag'], 'mnist', model_hash='vgg16' )
    return cfg, imageset

def get_cifar_pair_imageset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'cifar10_pair'
    params['nclasses'] = 10
    params['batch_size'] = 16
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    imageset = imageset_cifar10_pair(cfg, randomize=True, is_training=True, sample_num_pos=10000)
    #cfg.build_path( params['tag'], 'mnist', model_hash='vgg16' )
    return cfg, imageset

def get_cifar_imageset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'pair_patch_vgg16'
    params['nclasses'] = 13
    params['batch_size'] = 16
    params['gpu'] = '0'
    params['tag'] = '20190704'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    imageset = imageset_cifar10(cfg, randomize=True, is_training=True)
    cfg.build_path( params['tag'], 'cifar10', model_hash='vgg16' )
    return cfg, imageset

def get_image_set(cfg, dataset):

    #set the paths to save all the results (model, val result)
    blobs_gen = patch_pair_blobs(cfg.dnn)
    train_imageset = imageset_retrieval(cfg, dataset, blobs_gen, is_training=True, randomize=True)

    #train_feeder = data_feeder(train_imageset)

    return train_imageset

#def get_input(train_feeder):
#    data = train_feeder.next()
#    for key in data.keys():
#        print(key)
#        for a_key in data[key].keys():
#            print('Key: {}-{}, shape: {}'.format(key, a_key, data[key][a_key].shape))
#    return data

def get_model_device(cfg):
    params = {}
    params['gpu'] = '0'

    device = torch_tools.get_device( params['gpu'] )

    model = {}
    model['net'] = networks.pair_roi.net(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)
    print("Model's state_dict:")
    for param_tensor in model['net'].state_dict():
        print(param_tensor, "\t", model['net'].state_dict()[param_tensor].size())

    return model, device

def set_device( blobs, device ):
    for n,d in blobs.items() :
        if type(d) == list:
            for i, item in enumerate(d):
                blobs[n][i] = item.to(device)
        else:
            blobs[n] = d.to(device)
    return blobs

def get_input(dataset, device):
    inputs, targets = dataset.next()
    inputs = set_device( inputs, device)
    targets = set_device( targets, device )
    return inputs, targets

def get_imageset():
    cfg, dataset =get_dataset()
    imageset = get_image_set(cfg, dataset)
    return cfg, imageset

def do_stuff(cfg, imageset, model, device):
    #model, device = get_model_device(cfg)
    inputs, targets = get_input(imageset, device)
    x= model['net'](inputs)
    print(x['match'].shape)
    print(x['category'].shape)
    print(x['embeddings'].shape)
    return targets

def draw_fashion_box(imageset):
    inputs, targets = imageset.next()
    #print(targets['uid'].numpy())
    if 'images' in inputs:
        images = inputs['images']
    else:
        images = inputs['data']
    ori_images = inputs['ori_images']
    boxes = inputs['boxes']
    boxes = [box.numpy() for box in boxes]
    out_root = 'test_im'
    for i,image in enumerate(images):
        draw = Draw(image)
        box = boxes[i]
        for abox in box:
            draw.rectangle(list(abox), outline=(255, 0, 255))
        im_path = os.path.join(out_root, '{}.jpg'.format(i+1))
        image.save(im_path)
        im_path1 = os.path.join(out_root, '{}_ori.jpg'.format(i+1))
        ori_images[i].save(im_path1)


def draw_fashion_box_pair(imageset):
    inputs, targets = imageset.next()
    images = inputs['images']
    boxes = inputs['boxes']
    boxes = [box.numpy() for box in boxes]
    boxes = [np.concatenate((box, (i)*np.ones((len(box),1))), axis=-1) for i,box in enumerate(boxes)]
    boxes = np.concatenate(boxes, axis=0).astype(int)
    pair_inds = inputs['pair_indexes'].numpy()
    pair_labels = targets['pair_labels'].numpy()
    out_root = 'test_im'
    pair_text = ['Negative', 'Positive']
    for i, pair in enumerate(pair_inds):
        box1 = boxes[pair[0]]
        box2 = boxes[pair[1]]
        pair_label = pair_labels[i]
        im1_ind = box1[4]
        im2_ind = box2[4]
        crop1 = images[im1_ind].crop(box1[:4])
        crop2 = images[im2_ind].crop(box2[:4])
        draw1 = Draw(crop1)
        draw2 = Draw(crop2)
        draw1.text((0, 0),pair_text[pair_label],(255,255,255))
        draw2.text((0, 0),pair_text[pair_label],(255,255,255))
        im1_path = os.path.join(out_root, '{}_1.jpg'.format(i))
        im2_path = os.path.join(out_root, '{}_2.jpg'.format(i))
        crop1.save(im1_path)
        crop2.save(im2_path)

def count_box(images,dataset):
    box_num = 0
    for image in images:
        if image.source == 'user':
            for uid in image.unique_ids:
                if uid in dataset.valid_uids:
                    box_num+=1
    print('box num is {}'.format(box_num))

def get_vailid_unique_id(imageset):
    unique_id_stat = np.zeros(imageset._last_id+1, dtype=int)
    for image in imageset._images:
        if image.source=='user':
            continue
        for unique_id in image.unique_ids:
            unique_id_stat[unique_id] +=1
    unique_id_stat[0] = 0 # the id 0 are non-valid ids
    imageset.valid_uids = np.where(unique_id_stat>=1)[0]




if __name__=="__main__" :
    cfg, imageset = get_fashion_dataset()
    #do_stuff(cfg, imageset)
    #imageset = get_image_set(cfg, dataset)
    a = imageset.next()
    #print(a)
    #params = vars( args )

    #params['nclasses'] = 3
    #params['ncategories'] = 46

    #params['dset'] = ['deepfashion2','capb','train']
    #params['prefix'] = 'DeepfashionCap'

    #cfg = config( params['config'], 'train', params['datasetpath'], params['projectpath'] )
    #cfg.update( params )

    #train_dataset = load_dataset( cfg, ['deepfashionp','capb','train'] )
    ##test_dataset = load_dataset( cfg, ['deepfashionp','capb','test'], settings=[] )

    #cfg.build_path( params['tag'], train_dataset.hash )

    #preprocess = []
    #preprocess.append(transforms.ToTensor())
    #preprocess.append(transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]))

    #blobs_gen = blobs[cfg.dnn.NETWORK.INTERFACE]( cfg, preprocess=preprocess )

    #train_batches = batch_generator( cfg, train_dataset, blobs_gen, randomize=True, is_training=True )
    #train_feeder = data_feeder( train_batches )

    #use_cuda = torch.cuda.is_available()
    #device_name = "cuda" if use_cuda else "cpu"
    #device = torch.device( device_name )

    #model = {}
    #model['net'] = networks['gtbox'].Net( cfg ).to( device )
    #model['loss'] = networks['gtbox'].Loss( cfg ).to( device )

    #t = trainer( cfg, model, device, train_feeder )
    #t.train()

    #train_feeder.exit()

    #torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
