import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
from tools.visulize_tools import draw_loss, draw_accuracy


#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw
import tensorflow as tf

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data import pair_blobs, imageset_retrieval, imageset_fashion_mnist, imageset_mnist_pair, imageset_mnist_kgen
from rcnn_dnn.networks import networks, pair_patch, category_easy, pair_mnist

import torch
import torchvision.transforms as transforms
import torch.optim as optim
from benchmark import Benchmark
from rcnn_dnn.benchmark.classification_bench import RetrievalClassificationBench

from dnn import trainer
from draw_2d_scatter import draw_fashion_mnist


def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, benchmark=None, trainset_kgen=None, criterias=None,feature_set=None):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model
        self._feature_set = feature_set

        self._trainset = trainset
        self._testset = testset
        self._trainset_kgen = trainset_kgen
        self._benchmark = benchmark
        self._criterias = criterias
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()

        self.init_logger()

    def train( self ):
        if self._testset is not None :
            self._validate()

        for i in range( self._niter ):
            print("Epoch %d/%d" % (i+1,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None :
                self._validate()

    def _validate( self ):
        print('start to validate')
        self._model['net'].eval()

        self.get_k_vecs()
        bench = RetrievalClassificationBench(self._cfg, criterias, self._k_vecs)

        with torch.no_grad() :
            test_len = len(self._testset)
            for idx in range( test_len ):
                inputs, targets = self._testset.next()
                inputs = self._set_device( inputs )
                output = self._model['net']( inputs )
                bench.update( targets, output )
                if idx % 1000 == 0:
                    print('{}  test {}/{}'.format(datetime.datetime.now(),idx, test_len))

        bench.summary()

    def get_k_vecs(self):
        self._model['net'].eval()
        self._k_vecs = []
        for idx in range( len(self._trainset_kgen) ):
            inputs, targets = self._trainset_kgen.next()
            inputs = self._set_device( inputs )
            outputs = self._model['net']( inputs, just_embedding=True)
            self._k_vecs.append(outputs['embeddings'].detach().cpu().numpy())

    def _train( self ):
        self._model['net'].train()

        #widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
        #            '(',progressbar.DynamicMessage('loss'),')' ]
        #bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []
        average_losses = {}

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            outputs = self._model['net']( inputs)
            losses = self._model['loss']( outputs, targets )
            # add the losses for each part
            loss_sum = 0
            for single_loss in losses:
                loss_sum += losses[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= losses[single_loss]
                else:
                    average_losses[single_loss] = losses[single_loss]


            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%1000 == 0:
                self._logger.info('{} '.format(idx+1))
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 1000
                    self._logger.info('{} '.format(loss_num))
                    print(' {} loss:{}'.format(loss, loss_num))
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        for n,d in blobs.items() :
            if type(d) == list:
                for item in d:
                    self._set_device(item)
            else:
                blobs[n] = d.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model['net'].parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model['net'].parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def get_features(self):
        #model_path = cfg.path.MODEL
        #self.load_trained_model(model_path)
        self._model['net'].eval()

        with torch.no_grad() :
            test_len = len(self._feature_set)
            for idx in range( test_len ):
                inputs, targets = self._feature_set.next()
                label = targets['category'].detach().cpu().numpy()
                inputs = self._set_device( inputs )
                output = self._model['net']( inputs, just_embedding=True)
                feature = output['embeddings'].detach().cpu().numpy()
                if idx == 0:
                    feature_all = feature
                    labels = label
                else:
                    feature_all = np.concatenate((feature_all,feature), axis=0)
                    labels = np.concatenate((labels,label), axis=0)
        return {'features':feature_all, 'labels':labels}

    def save_features(self):
        features = self.get_features()
        feature_file = 'fashion_mnist_feature/feature_{}d_fashion_mnist.pkl'.format(self._cfg.dnn.NETWORK.FEATURE_DIM)
        with open(feature_file,'wb') as f:
            pickle.dump(features, f)
            print('features are saved to {}'.format(feature_file))




def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    params = {}
    params['config'] = 'mnist_pair'
    params['batch_size'] = 64
    params['nclasses'] = 10
    params['gpu'] = '0'
    params['tag'] = '20190808_fashion_mnist_D_match_64d'
    params['path'] = {}
    
    k_list = [5, 10, 20]

    #criterias = ['match']
    criterias = ['category_accuracy', 'pair_match_accuracy', 'retrieval_classification_accuracy']
    criterias = ['category_accuracy', 'pair_match_accuracy' ]
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    #test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'mnist', model_hash='EasyNetD' )

    #benchmark = pair_mnist.Benchmark(cfg, criterias=criterias)
    #benchmark = Benchmark(cfg, criterias)
    benchmark = None

    test_imageset = imageset_mnist_pair(cfg, is_training=False, randomize=False, sample_num_pos=4000 )
    train_imageset = imageset_mnist_pair(cfg, is_training=True, randomize=True, sample_num_pos=10000)
    train_imageset_kgen = imageset_mnist_kgen(cfg, k_list)
    feature_set = imageset_fashion_mnist(cfg, randomize=False, is_training=False)

    #train_feeder = data_feeder(train_imageset)
    #test_feeder = data_feeder(test_imageset)
    #test_feeder = None

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    model['net'] = networks.pair_mnist.net( cfg ).to( device )
    model['loss'] = networks.pair_mnist.loss(cfg).to(device)
    #model['net'] = networks.category_embedding.net(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)

    t = my_trainer( cfg, model, device, train_imageset, testset=test_imageset, 
                    trainset_kgen=train_imageset_kgen,benchmark=benchmark, criterias=criterias,
                    feature_set = feature_set )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    t.train()

    #t.load_trained_model(cfg.path.MODEL)
    t.save_features()

    #loss_log_path = cfg.path['LOG']
    #loss_names = ['category_loss']
    #out_path = 'test/{}_loss.jpg'.format(params['tag'])
    #draw_loss(loss_log_path,out_path)

    #bench_log_path = cfg.path['BENCHMARK_LOG']
    #out_path = 'test/{}_benchmark.jpg'.format(params['tag'])
    #draw_accuracy(bench_log_path, out_path)

    #train_feeder.exit()
    #test_feeder.exit()

    torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)

    draw_fashion_mnist(cfg.dnn.NETWORK.FEATURE_DIM)
