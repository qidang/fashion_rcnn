#import sys
#sys.path.append('../')
import _init
import numpy as np
import h5py
import platform
import random
import itertools
import time
import os
import pickle
#from data.dataset import deepfashion_dataset
#from data import workbench
from tools import torch_tools
from rcnn_config import config
from rcnn_dnn.data import patch_blobs
from data.datasets import load_dataset
import torchvision.transforms as transforms

class hdf5_saver:
    def __init__(self, trainset, testset, save_path,  patch_size):
        self._trainset = trainset
        self._testset = testset
        self._save_path = save_path
        self._patch_size = patch_size
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def set_unique_id(self, dataset):
        images = dataset.images
        # set unique id for each image
        pair_id_dict = {}
        unique_id = 1
        for image in images:
            image.unique_ids = []
            if image.pair_id not in pair_id_dict:
                pair_id_dict[image.pair_id] = {}
            for style in image.styles:
                if style == 0:
                    image.unique_ids.append(0)
                    continue
                else:
                    if style in pair_id_dict[image.pair_id]:
                        image.unique_ids.append(pair_id_dict[image.pair_id][style])
                    else:
                        pair_id_dict[image.pair_id][style] = unique_id
                        image.unique_ids.append(unique_id)
                        unique_id += 1
        last_id = unique_id - 1
        #self.check_unique_id(images, last_id)
        return last_id

    def check_unique_id(self, images, last_id):
        shop = np.zeros(last_id)
        user = np.zeros(last_id)
        for image in images:
            for unique_id in image.unique_ids:
                if image.source == 'shop':
                    shop[unique_id-1] = 1
                elif image.source == 'user':
                    user[unique_id-2] = 1
                else:
                    print('error in pair list')
        empty_shop = np.where(shop==0)
        empty_user = np.where(user==0)
        print('empty_shop unique ids are : {}'.format(empty_shop))
        print('empty_shop unique ids are : {}'.format(empty_user))
            
    def count_box(self, dataset):
        images = dataset.images
        box_num = 0
        for image in images:
            box_num+=len(image.boxes)
        return box_num

    def save_dataset(self, dataset, h5_file, blobs_gen, patch_size, name='train'):
        last_id = self.set_unique_id(dataset)
        ndata = self.count_box(dataset)
        data = h5_file.create_dataset('{}_data'.format(name), shape=(ndata, 3, patch_size, patch_size), dtype=np.float32)
        category = h5_file.create_dataset('{}_category'.format(name), shape=(ndata,), dtype=np.int)
        unique_id = h5_file.create_dataset('{}_unique_id'.format(name), shape=(ndata,), dtype=np.int)
        source = h5_file.create_dataset('{}_source'.format(name), shape=(ndata,), dtype=np.int)
        unique_id_max = h5_file.create_dataset('{}_unique_id_max'.format(name), shape=(1,), dtype=np.int)
        unique_id_max[0] = last_id # unique id are range from 1 to unique id max (include max)

        images = dataset.images
        ind=0
        source_dict = {'user':0, 'shop':1}
        for image in images:
            patches = self.get_patches(image)
            categories = image.categories.ravel()
            unique_ids = image.unique_ids
            item_source = image.source
            for i, patch in enumerate(patches):
                data[ind] = patch
                category[ind] = categories[i]
                unique_id[ind] = unique_ids[i]
                source[ind] = source_dict[item_source]
                ind += 1
        assert ind == ndata

    def get_patches(self, image):
        data = []
        im_size = self._patch_size

        im = image.im_PIL.convert('RGB')
        for box in image.boxes:

            box = np.floor( box ).astype( int )

            # Extracting the patch
            patch = im.crop((box[0],box[1],box[2],box[3]))
            patch = patch.resize((im_size, im_size))

            pt_patch = self._toTensor( patch )
            pt_patch = self._normalizeTensor( pt_patch )

            data.append( pt_patch )
        return data


    def save_data( self ):
        trainset = self._trainset
        testset = self._testset
        patch_size = self._patch_size

        with h5py.File(self._save_path, 'w') as h5 :
            self.save_dataset(trainset, h5, blobs_gen, patch_size, name='train')
            self.save_dataset(testset, h5, blobs_gen, patch_size, name='test')

        print( 'The h5 file is saved to {}'.format(self._save_path) )

def get_dataset():
  #args = parse_commandline()

    params = {}
    params['config'] = 'pair_patch_vgg16'
    params['batch_size'] = 256
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    train_dataset = load_dataset( cfg, ['deepfashion2','train'] )
    test_dataset = load_dataset( cfg, ['deepfashion2','validation'] )


    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    #load the dataset
    #testbench.load_dataset([])

    #test_set = testbench.dataset
    return cfg, train_dataset, test_dataset

if __name__=="__main__" :
    cfg, train_set, test_set = get_dataset()

    patch_size = 128
    train_path = cfg.path.HDF5 % ( 'DeepFashion2_patch_{}'.format(patch_size) )
    save_path = train_path
    #save_path = '/hdd/data/datasets/DeepFashion2_hdf5/DeepFashion2_patch_{}.hdf5'.format(patch_size)
    blobs_gen = patch_blobs(cfg.dnn)
    saver = hdf5_saver(train_set, test_set, save_path, patch_size)
    saver.save_data()
    #save_data(train_set, test_set, train_path, blobs_gen, patch_size)

    #blobs_gen = patch_blobs( cfg )
    #trainset = dataset( trainbench.dataset.images, blobs_gen,
    #                    batch_size=cfg.dnn.NETWORK.BATCH_SIZE,
    #                    randomize=True, training=True )

    #testset = dataset( testbench.dataset.images, blobs_gen,
    #                   batch_size=cfg.dnn.NETWORK.BATCH_SIZE,
    #                   training=False,
    #                   benchmark=networks.landmarks_patch.benchmark )
    ##testset = None

    #model = {}
    #model['net'] = networks.landmarks_patch.net( cfg ).to( device )
    #model['loss'] = networks.landmarks_patch.loss( cfg ).to( device )

    #criterias = ['accuracy', 'NE']
    #tt = trainer( cfg, model, device, trainset, testset, criterias )
    ##tt = trainer( cfg, model, device, trainset )
    ##tt.test()
    #tt.train()

    #torch.save( model['net'].state_dict(), cfg.path.MODEL )
    #print('Saved to %s' % (cfg.path.MODEL))
